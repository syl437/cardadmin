// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js

angular.module('starter', ['ionic', 'starter.controllers','ngStorage','ngCordova','starter.factories','google.places', '720kb.tooltips'])

.config(function($ionicConfigProvider) {
  $ionicConfigProvider.scrolling.jsScrolling(true);
})


.run(function($ionicPlatform,$rootScope,$http,$localStorage, $window,$state,$ionicLoading) {
  $rootScope.LaravelHost = 'http://bamapa.co.il/laravel/public/';
  $rootScope.Host = 'http://bamapa.co.il/';
  $rootScope.ShopHost = 'http://bamapa.co.il/php/' //'http://www.mystor.co.il/php/'; //
  $rootScope.currentSetting = 0;
  $rootScope.fontColors = new Array("#ffffff","#020202","#f2aa1a","#989898","#2cdda9","#cfd3d2","#6c60a1","#be5f03","#6d548d","#3e3d3e");
  $rootScope.lineColors = new Array("white","black","green","red","blue");
  $rootScope.NameColors = new Array("#ffffff","#020202","#f2aa1a","#989898","#2cdda9","#cfd3d2","#6c60a1","#be5f03","#6d548d","#3e3d3e");
  $rootScope.BasketArray = [];
  $rootScope.loadOnce = 0;
  $rootScope.productPage = "#/app/shopProducts/";
	// $rootScope.fonts = new Array("Alef","Amatica SC","Arimo","Assistant","Cousine","David Libre","Frank Ruhl Libre","Heebo","Miriam Libre","Open Sans","Rubik","Secular One","Suez One","Tinos","Varela Round");
	$rootScope.fonts = new Array("Ariel","Alef","Rubik");
	$rootScope.DealsArray = [];
	$rootScope.currState = $state;
	$rootScope.State = '';
	$rootScope.SearchMainArray = [];
	$rootScope.currentJsonIndex = "";
   
	$rootScope.$watch('currState.current.name', function(newValue, oldValue) {
      $rootScope.State = newValue;
    }); 

	
  
  $ionicPlatform.ready(function() {

  
	$rootScope.getCategories = function()
	{
		params = {};
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';	
		$rootScope.URL = $rootScope.LaravelHost+'/GetCategories'; 
		$http.post($rootScope.URL,params)
		.success(function(data, status, headers, config)
		{
			$rootScope.Catagories = data;
			console.log("Catagories: " , data);
		})
		.error(function(data, status, headers, config)
		{
		
		});					
	}
	

	$rootScope.getCategories();
	
	
	 $rootScope.getShopCatagories = function()
	  {
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
		
		data1 = 
		{
			"categoryId" : $localStorage.custumerid 
		}				
		$http.post($rootScope.ShopHost+'/getCategory.php',data1)
		.success(function(data, status, headers, config)
		{
			$ionicLoading.hide();
			$rootScope.catArray = data;
			console.log("catagories: " , $localStorage.custumerid )
			
			$rootScope.CatagoriesLoaded++;	
			console.log("products: " , data);
		})
		.error(function(data, status, headers, config)
		{
			$ionicLoading.hide();
		});	
	  }
	  
	  
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
	

	
  });
})

.config(function($stateProvider, $urlRouterProvider,$ionicConfigProvider) {
	$ionicConfigProvider.backButton.previousTitleText(false).text('');
  $stateProvider

    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })

  
    .state('app.main', {
      url: '/main',
      views: {
        'menuContent': {
          templateUrl: 'templates/main.html',
          controller: 'MainCtrl'
        }
      }
    })
	
    .state('app.managecard', {
      url: '/managecard',
      views: {
        'menuContent': {
          templateUrl: 'templates/managecard.html',
          controller: 'ManageCardCtrl'
        }
      }
    })

    .state('app.search', {
      url: '/search/:ItemId',
      views: {
        'menuContent': {
          templateUrl: 'templates/search.html',
          controller: 'SearchCtrl'
        }
      }
    })
	


	

    .state('app.details', {
      url: '/details/:ItemId',
      views: {
        'menuContent': {
          templateUrl: 'templates/details.html',
          controller: 'DetailsCtrl'
        }
      }
    })


    .state('app.realestate', {
      url: '/realestate/:ItemId',
      views: {
        'menuContent': {
          templateUrl: 'templates/realestate.html',
          controller: 'RealEstateCtrl'
        }
      }
    })

	
	
	.state('app.shopIntro', {
      url: '/shopIntro/:ItemId',
      views: {
        'menuContent': {
          templateUrl: 'templates/shopIntro.html',
          controller: 'shopIntroCtrl'
        }
      }
    })
	
	.state('app.shopProducts', {
      url: '/shopProducts/:ItemId',
      views: {
        'menuContent': {
          templateUrl: 'templates/shopProducts.html',
          controller: 'shopProductsCtrl'
        }
      }
    })
	
	.state('app.basket', {
      url: '/basket',
      views: {
        'menuContent': {
          templateUrl: 'templates/basket.html',
          controller: 'BasketCtrl'
        }
      }
    })	
	
	.state('app.payment', {
      url: '/payment',
      views: {
        'menuContent': {
          templateUrl: 'templates/payment.html',
          controller: 'PaymentCtrl'
        }
      }
    })	
	
	.state('app.shopDetails', {
      url: '/shopDetails/:Type/:ItemId/:CatagoryId',
      views: {
        'menuContent': {
          templateUrl: 'templates/shopDetails.html',
          controller: 'shopDetailsCtrl'
        }
      }
    })
	
	

    .state('app.login', {
      url: '/login',
      views: {
        'menuContent': {
          templateUrl: 'templates/login.html',
          controller: 'LoginRegisterCtrl'
        }
      }
    })	
	
    .state('app.forgotpass', {
      url: '/forgotpass',
      views: {
        'menuContent': {
          templateUrl: 'templates/forgotpass.html',
          controller: 'LoginRegisterCtrl'
        }
      }
    })	
	
    .state('app.forgotstore', {
      url: '/forgotstore',
      views: {
        'menuContent': {
          templateUrl: 'templates/forgotstore.html',
          controller: 'LoginRegisterCtrl'
        }
      }
    })		
	
	

	

    .state('app.loginshop', {
      url: '/loginshop',
      views: {
        'menuContent': {
          templateUrl: 'templates/loginshop.html',
          controller: 'LoginRegisterCtrl'
        }
      }
    })	
	
	
	

	
	 .state('app.contact', {
      url: '/contact',
      views: {
        'menuContent': {
          templateUrl: 'templates/contact.html',
          controller: 'ContactCtrl'
        }
      }
    })	


    .state('app.register', {
      url: '/register',
      views: {
        'menuContent': {
          templateUrl: 'templates/register.html',
          controller: 'LoginRegisterCtrl'
        }
      }
    })	

    .state('app.registershop', {
      url: '/registershop',
      views: {
        'menuContent': {
          templateUrl: 'templates/registershop.html',
          controller: 'LoginRegisterCtrl'
        }
      }
    })	

    .state('app.loginsettings', {
      url: '/loginsettings',
      views: {
        'menuContent': {
          templateUrl: 'templates/loginsettings.html',
          controller: 'LoginRegisterCtrl'
        }
      }
    })		
	
	
    .state('app.managedeals', {
      url: '/managedeals',
      views: {
        'menuContent': {
          templateUrl: 'templates/managedeals.html',
          controller: 'ManageDealsCtrl'
        }
      }
    })	

    .state('app.dealdetails', {
      url: '/dealdetails/:ItemId',
      views: {
        'menuContent': {
          templateUrl: 'templates/dealdetails.html',
          controller: 'DealDetailsCtrl'
        }
      }
    })	
	
    .state('app.newdeal', {
      url: '/newdeal/:ItemId',
      views: {
        'menuContent': {
          templateUrl: 'templates/newdeal.html',
          controller: 'NewDealCtrl'
        }
      }
    })	


    .state('app.deals', {
      url: '/deals/:ItemId',
      views: {
        'menuContent': {
          templateUrl: 'templates/deals.html',
          controller: 'DealsCtrl'
        }
      }
    })	

    .state('app.hotdeals', {
      url: '/hotdeals',
      views: {
        'menuContent': {
          templateUrl: 'templates/hotdeals.html',
          controller: 'HotDealsCtrl'
        }
      }
    })		
	
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/main');
});
