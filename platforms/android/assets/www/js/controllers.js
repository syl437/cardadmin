angular.module('starter.controllers', [])


.controller('AppCtrl', function($scope, $ionicModal, $timeout , $rootScope,SendPostToServer,$ionicHistory,$ionicPopup,$localStorage,$ionicSideMenuDelegate) {

	$scope.areas = [];
	$scope.areas[1] = "צפון";
	$scope.areas[2] = "חיפה והסביבה";
	$scope.areas[3] = "קריות והסביבה";
	$scope.areas[4] = "עכו - נהריה והסביבה";
	$scope.areas[5] = "גליל עליון";
	$scope.areas[6] = "הכנרת והסביבה";
	$scope.areas[7] = "כרמיאל והסביבה";
	$scope.areas[8] = "נצרת - שפרעם והסביבה";
	$scope.areas[9] = "ראש פינה החולה";
	$scope.areas[10] = "גליל תחתון";
	$scope.areas[11] = "הגולן";
	$scope.areas[12] = "חדרה זכרון ועמקים";
	$scope.areas[13] = "זכרון וחוף הכרמל";
	$scope.areas[14] = "חדרה והסביבה";
	$scope.areas[15] = "קיסריה והסביבה";
	$scope.areas[16] = "יקנעם טבעון והסביבה";
	$scope.areas[17] = "עמק בית שאן";
	$scope.areas[18] = "עפולה והעמקים";
	$scope.areas[19] = "רמת מנשה";
	$scope.areas[20] = "השרון";
	$scope.areas[21] = "נתניה והסביבה";
	$scope.areas[22] = "רמת השרון - הרצליה";
	$scope.areas[23] = "רעננה - כפר סבא";
	$scope.areas[24] = "הוד השרון והסביבה";
	$scope.areas[25] = "דרום השרון";
	$scope.areas[26] = "צפון השרון";
	$scope.areas[27] = "מרכז";
	$scope.areas[28] = "תל אביב";
	$scope.areas[29] = "תל אביב - מרכז";
	$scope.areas[30] = "תל אביב - צפון";
	$scope.areas[31] = "תל אביב - דרום";
	$scope.areas[32] = "תל אביב -  מזרח";
	$scope.areas[33] = "ראשון לציון והסביבה";
	$scope.areas[34] = "חולון - בת ים";
	$scope.areas[35] = "רמת גן - גבעתיים";
	$scope.areas[36] = "פתח תקווה והסביבה";
	$scope.areas[37] = "ראש העין והסביבה";
	$scope.areas[38] = "בקעת אונו";
	$scope.areas[39] = "רמלה - לוד";
	$scope.areas[40] = "בני ברק - גבעת שמואל";
	$scope.areas[41] = "עמק  איילון";
	$scope.areas[42] = "שוהם והסביבה";
	$scope.areas[43] = "מודיעין והסביבה";
	$scope.areas[44] = "אזור ירושלים";
	$scope.areas[45] = "ירושלים",
	$scope.areas[46] = "בית שמש והסביבה";
	$scope.areas[47] = "הרי יהודה - מבשרת והסביבה";
	$scope.areas[48] = "מעלה אדומים והסביבה";
	$scope.areas[49] = "יהודה שומרון ובקעת הירדן";
	$scope.areas[50] = "ישובי דרום ההר";
	$scope.areas[51] = "ישובי שומרון";
	$scope.areas[52] = "גוש עציון";
	$scope.areas[53] = "בקעת הירדן וצפון ים המלח";
	$scope.areas[54] = "אריאל וישובי יהודה";
	$scope.areas[55] = "שפלה מישור חוף דרומי";
	$scope.areas[56] = "נס ציונה - רחובות";
	$scope.areas[57] = "אשדוד - אשקלון והסביבה";
	$scope.areas[58] = "גדרה - יבנה והסביבה";
	$scope.areas[59] = "קרית גת והסביבה";
	$scope.areas[60] = "שפלה";
	$scope.areas[61] = "דרום";
	$scope.areas[62] = "באר שבע והסביבה";
	$scope.areas[63] = "אילת וערבה";
	$scope.areas[64] = "ישובי הנגב";
	$scope.areas[65] = "הנגב המערבי";
	$scope.areas[66] = "דרום ים המלח";
	$scope.areas[67] = "חו\"ל";
	
  
  //$scope.getAreaName(0);
  
  $scope.getAreaName = function(id)
  {
	  return ($scope.areas[id]);
  }
  
  
  


	$scope.sharefields =
	{
		"name" : "",
		"phone" : ""
	}
	
	$scope.currentPage = $rootScope.State;

	$rootScope.$watch('currState.current.name', function(newValue, oldValue) {
      $scope.currentPage = newValue;
    }); 
	
	$scope.checkConnectedStatus = function()
	{
		if ($localStorage.userid)
			$scope.connectedUser = true;
		else
			$scope.connectedUser = false;
	}
	
	$scope.checkConnectedStatus();
	
	if ($localStorage.userid)
		$scope.loggedinUsername = $localStorage.name;
	else
		$scope.loggedinUsername = '';
	
	
	$scope.changeCatagory = function(cat,index)
	{
		$ionicHistory.nextViewOptions({
			disableAnimate: true,
			expire: 300
		});
		window.location.href = $rootScope.productPage+index;
	}
	
	
	
	
	
	 $scope.$on('$ionicView.enter', function(e) 
	 {
		 	$scope.CurrentSetting = 0; 
			
			$scope.settings = function(num)
			{
				$scope.CurrentSetting = num;
				$rootScope.currentSetting = num	
			}	 
	 })
	 
	 
	 $scope.goMain = function()
	 {
		 window.location ="#/app/main";
	 }
	 
	 $scope.goManageCard = function()
	 {
		 window.location ="#/app/managecard";
	 }
	 
	 $scope.goSettings = function()
	 {
		 window.location ="#/app/loginsettings";
	 }
	 
	 
	$scope.ShareBtn = function()
	{
		  $ionicModal.fromTemplateUrl('templates/share_modal.html', {
			scope: $scope
		  }).then(function(ShareModal) {
			$scope.ShareModal = ShareModal;
			$scope.ShareModal.show();
		  });		
	}
	
	$scope.closeShareModal = function()
	{
		$scope.ShareModal.hide();
	}
	
	$scope.sendShare = function()
	{
		if ($scope.sharefields.name =="")
		{
			$ionicPopup.alert({
			 title: 'יש להזין שם מלא',
			 template: ''
		   });				
		}
		else if ($scope.sharefields.phone =="")
		{
			$ionicPopup.alert({
			 title: 'יש להזין טלפון',
			 template: ''
		   });				
		}
		else
		{
			params = 
			{
				"user" : $localStorage.userid,
				"sub" : $localStorage.subdomain,
				"name" : $scope.sharefields.name,
				"phone" : $scope.sharefields.phone,
				
			};
		
			SendPostToServer(params,$rootScope.LaravelHost+'/ShareCard',function(data, success) 
			{

				$ionicPopup.alert({
				 title: 'תודה! לינק שיתוף נשלח בהצלחה',
				 template: ''
			   });
		   
				$scope.sharefields.name = '';
				$scope.sharefields.phone = '';
				$scope.closeShareModal();
			});	
		}
		
	}	
	
	$scope.logOut = function()
	{
		$scope.isLoggedIn = false;
		$localStorage.userid = '';
		$localStorage.name  = '';
		$localStorage.subdomain  = '';
		
		//disconnect shop user as well
		$localStorage.shopuserid = '';
		$localStorage.shopusername = '';
						
		$scope.checkConnectedStatus();
		$ionicSideMenuDelegate.toggleRight();
	}	
	
	$scope.logIn = function()
	{
		window.location ="#/app/login";
	}
	
	$scope.contactUs = function()
	{
		window.location ="#/app/contact";
	}
	


	
})

.controller('MainCtrl', function($scope,$rootScope,$localStorage,$ionicSideMenuDelegate,SendPostToServer,SetDetailstoArray,SwipeLeftRight,$ionicPopup,$ionicModal) 
{
	$scope.host = $rootScope.Host+'/php/';
	$scope.Catagories = $rootScope.Catagories;
	
	$scope.fields = 
	{
		"title" : "",
		"location_lat" : "",
		"location_lng" : "" 
	}

	if (typeof subdomain !== 'undefined') {
		
		if (subdomain != "www" && subdomain != "bamapa")
			window.location ="#/app/details/"+subdomain;
	}

	
	
	$rootScope.$watch('Catagories', function () 
	{
		$scope.Catagories = $rootScope.Catagories 
  	})
  
	if ($localStorage.userid)
		$scope.isLoggedIn = true
	else
		$scope.isLoggedIn = false;
	
	

	$scope.isLogin = 0;
	if (!$localStorage.userid)
		$scope.isLogin = 0;
	else
		$scope.isLogin  = 1;
		
	
	$scope.navigateCat = function(index,id)
	{
		$rootScope.currentJsonIndex = index;
		window.location ="#/app/search/"+id;
	}
	
	$scope.checkLoggedIn = function()
	{
		if (!$localStorage.userid)
		{
			window.location ="#/app/register";
			$scope.isLogin = 0;
		}
		else
		{
			window.location ="#/app/managecard";
			$scope.isLogin  = 1;
		}
	}
	
	$scope.doSearch = function()
	{
		if ($scope.fields.title =="")
		{
			$ionicPopup.alert({
			 title: 'יש להזין שם העסק',
			 template: ''
		   });	
		}
		else
		{
			alert (444);
			return;
			
			var optionsPosition = {
			  enableHighAccuracy: false,
			  //timeout: 5000,
			  maximumAge: 0
			};

					
			navigator.geolocation.getCurrentPosition($scope.successLocation, $scope.errorLocation, optionsPosition);

			
		}
	}
	
	
	
	$scope.successLocation = function(pos)
	{


		$scope.fields.location_lat = pos.coords.latitude;
		$scope.fields.location_lng = pos.coords.longitude
		
		$scope.searchMainFun();

	}
	
	$scope.errorLocation = function()
	{
		$scope.fields.location_lat = "";
		$scope.fields.location_lng = "";	
		$scope.searchMainFun();
	}
	
	$scope.searchMainFun = function()
	{
		SendPostToServer($scope.fields,$rootScope.LaravelHost+'/MainSupplierSearch',function(data, success) 
		{
			if (data.length == 0)
			{
				$ionicPopup.alert({
				 title: 'לא נמצאו תוצאות יש לנסות שוב',
				 template: ''
			   });						
			}
			else
			{
				$rootScope.SearchMainArray = data;
				console.log("search array:" , data);
				window.location ="#/app/search/0";
			}
			
				$scope.fields.title = '';
		});
	}

})
	


.controller('ContactCtrl', function($scope,$rootScope,$localStorage,$stateParams,$ionicSideMenuDelegate,SendPostToServer,SetDetailstoArray,SwipeLeftRight,$ionicPopup) 
{
	
})

.controller('LoginRegisterCtrl', function($scope,$rootScope,$localStorage,$stateParams,$ionicSideMenuDelegate,SendPostToServer,SetDetailstoArray,SwipeLeftRight,$ionicPopup) 
{
	$scope.Subcategories = [];

	$scope.login = 
	{
		"mail" : "",
		"password" : ""
	}
	
	$scope.register = 
	{
		"name" : "",
		"phone" : "",
		"address" : "",
		"subdomain" : "",
		"mail" : "",
		"password" : "",	
		"category" : "-1",
		"subcat" : "-1",
		"custumerid" : $localStorage.custumerid,
		"terms" : false
	}
	
	$scope.forgotfields = 
	{
		"email" : ""
	}
	
	$scope.settingsfields = 
	{
		"password" : ""
	}
	
	$scope.agreeTerms = function(state)
	{
		$scope.register.terms = state;
	}
	
	$scope.openTerms = function()
	{
		iabRef = window.open("http://bamapa.co.il/terms.pdf", '_system', 'location=yes');
	}
		
	
	
	$scope.$watch('register.category', function(value) 
	{
		if ($scope.register.category != "-1")
		{
			for(var i=0;i< $rootScope.Catagories.length;i++)
			{
				if ($rootScope.Catagories[i].index == $scope.register.category)
				{
					$scope.Subcategories = $rootScope.Catagories[i].subcategories;
				}
			}
		}
	  
	});	

	
	//type = 1 (store user login) else card user login
	$scope.doLogin = function(type)
	{	
		if (type == 1)
			$scope.URL = 'StoreUserLogin';
	    else
			$scope.URL = 'LoginByEmail';
		
		
		if ($scope.login.mail =="")
		{
			$ionicPopup.alert({
			 title: 'יש להזין דוא"ל',
			 template: ''
		   });				
		}
		else if ($scope.login.password =="")
		{
			$ionicPopup.alert({
			 title: 'יש להזין סיסמה',
			 template: ''
		   });				
		}
		else
		{
			SendPostToServer($scope.login,$rootScope.LaravelHost+'/'+$scope.URL,function(data, success) 
			{
				if (data.status == 0)
				{
					$ionicPopup.alert({
					 title: 'כתובת דוא"ל או סיסמה שגוים יש לנסות שוב',
					 template: ''
				   });	

					$scope.login.password = '';
				}
				else
				{
					if (type == 1)
					{
						$localStorage.shopuserid = data.userid;
						$localStorage.shopusername = data.name;
						window.location ="#/app/basket";
					}
					else
					{
						$localStorage.userid = data.userid;
						$localStorage.name = data.name;
						$localStorage.subdomain = data.subdomain;
						$scope.checkConnectedStatus();
						window.location ="#/app/managecard";						
					}
				}
			});				
		}
	}
	
	$scope.sendPass = function(type)
	{
		if (type == 0)
			$scope.URL = 'ForgotSupplierPass';
		else
			$scope.URL = 'ForgotStorePass';
		
		
		if ($scope.forgotfields.email =="")
		{
			$ionicPopup.alert({
			 title: 'יש להזין דוא"ל',
			 template: ''
		   });	
		}
		else
		{
			SendPostToServer($scope.forgotfields,$rootScope.LaravelHost+'/'+$scope.URL,function(data, success) 
			{
				if (data.status == 0)
				{
					$ionicPopup.alert({
					 title: 'כתובת מייל לא נמצאה יש לנסות שוב',
					 template: ''
				   });	

					$scope.forgotfields.email = '';
				}
				else
				{
					$ionicPopup.alert({
					 title: 'סיסמה נשלחה בהצלחה',
					 template: ''
				   });	

					if (type == 0)
						window.location ="#/app/login";	
					else
						window.location ="#/app/loginshop";		
				}
				
					$scope.forgotfields.email = '';
			});
		}	
	}
	
	$scope.saveLoginSettings = function()
	{
		if ($scope.settingsfields.password =="")
		{
			$ionicPopup.alert({
			 title: 'יש להזין סיסמה',
			 template: ''
		   });	
		}
		else
		{
			params = 
			{
				"user" : $localStorage.userid,
				"password" : $scope.settingsfields.password
				
			};
		
			SendPostToServer(params,$rootScope.LaravelHost+'/saveLoginSettings',function(data, success) 
			{

				$ionicPopup.alert({
				 title: 'סיסמה עודכנה בהצלחה',
				 template: ''
			   });
		   
				$scope.settingsfields.password = '';
				window.location ="#/app/main";	
			});	
		}
	}

	$scope.getCategories = function()
	{
		params = {};
		
		SendPostToServer(params,$rootScope.LaravelHost+'/GetCategories',function(data, success) 
		{
			$scope.Catagories = data;
		});		
	}
	

	$scope.getCategories();
	
	//type = 1 (store user register) else card user login
	$scope.doRegister = function(type)
	{
		var emailRegex = /\S+@\S+\.\S+/;
		console.log($scope.register)
		
		
		if (type == 1)
			$scope.URL = 'StoreUserRegister';
		else
			$scope.URL = 'RegisterByEmail';
		
		if ($scope.register.name =="")
		{
			$ionicPopup.alert({
			 title: 'יש להזין שם מלא',
			 template: ''
		   });				
		}
		
		else if ($scope.register.phone =="" && type == 1)
		{
			$ionicPopup.alert({
			 title: 'יש להזין מספר טלפון',
			 template: ''
		   });				
		}

		else if ($scope.register.address =="" && type == 1)
		{
			$ionicPopup.alert({
			 title: 'יש להזין כתובת למשלוח',
			 template: ''
		   });				
		}

		
		else if ($scope.register.subdomain =="" && type == 0)
		{
			$ionicPopup.alert({
			 title: 'יש להזין סאב דומיין',
			 template: ''
		   });				
		}
		else if ($scope.register.category =="-1" && type == 0)
		{
			$ionicPopup.alert({
			 title: 'יש לבחור קטגוריה',
			 template: ''
		   });				
		}	

		else if ($scope.register.subcat =="-1" && type == 0)
		{
			$ionicPopup.alert({
			 title: 'יש לבחור תת קטגוריה',
			 template: ''
		   });				
		}

		
		else if ($scope.register.mail =="")
		{
			$ionicPopup.alert({
			 title: 'יש להזין דוא"ל',
			 template: ''
		   });				
		}
		else if (emailRegex.test($scope.register.mail) == false)
		{
			$ionicPopup.alert({
			title: 'כתובת דוא"ל לא תקין יש לתקן',
			buttons: [{
			text: 'אישור',
			type: 'button-positive',
			  }]
		   });	

		   $scope.register.mail =  '';
		}			
		else if ($scope.register.password =="")
		{
			$ionicPopup.alert({
			 title: 'יש להזין סיסמה',
			 template: ''
		   });				
		}
		else if ($scope.register.terms == false)
		{
			$ionicPopup.alert({
			 title: 'יש להסכים לתנאי השימוש',
			 template: ''
		   });				
		}		
		else
		{

			SendPostToServer($scope.register,$rootScope.LaravelHost+'/'+$scope.URL,function(data, success) 
			{
				

				if (type == 1)
				{
					
					if (data.status == 0)
					{
						$ionicPopup.alert({
						 title: 'הרשמה בוצעה בהצלחה',
						 template: ''
					   });

					   
						$localStorage.shopuserid = data.userid;
						$localStorage.shopusername = $scope.register.name
						window.location ="#/app/basket";
						
						$scope.register.name =  '';	
						$scope.register.phone =  '';	
						$scope.register.address =  '';	
						$scope.register.mail =  '';	
						$scope.register.password =  '';		
					}
					else if (data.status == 1)
					{
						$ionicPopup.alert({
						 title: 'מייל כבר בשימוש יש לבחור מייל אחר',
						 template: ''
					   });	
					   
						$scope.register.mail =  '';							
					}
					
				}
				else
				{
					if (data.status == 0)
					{
						$ionicPopup.alert({
						 title: 'הרשמה בוצעה בהצלחה',
						 template: ''
					   });			

						$localStorage.userid = data.userid;
						$localStorage.name = $scope.register.name;
						$localStorage.subdomain = $scope.register.subdomain	;	   
						window.location ="#/app/managecard";
						
						$scope.register.name =  '';	
						$scope.register.phone =  '';	
						$scope.register.address =  '';	
						$scope.register.subdomain =  '';	
						$scope.register.mail =  '';	
						$scope.register.password =  '';	
						$scope.register.category =  '';	
						$scope.register.subcat =  '';	
						
					}
					else if (data.status == 1)
					{
						$ionicPopup.alert({
						 title: 'מייל כבר בשימוש יש לבחור מייל אחר',
						 template: ''
					   });	
					   
						$scope.register.mail =  '';	
					}
					else if (data.status == 2)
					{
						$ionicPopup.alert({
						 title: 'סאב דומיין בשימוש יש לבחור סאב דומיין אחר',
						 template: ''
					   });	
					   
						$scope.register.subdomain =  '';						
					}					
				}

				

			});	
		}
	}
})	
	
.controller('SearchCtrl', function($scope,$state,$rootScope,$localStorage,GetShopById,$stateParams,$ionicSideMenuDelegate,SendPostToServer,SetDetailstoArray,SwipeLeftRight,$ionicScrollDelegate,$ionicPopup) 
{
	$scope.host = $rootScope.Host+'/php/';
	$scope.ItemId = $stateParams.ItemId;
	$scope.currentId = "";
	$scope.oldId = "";
	$scope.data = "";
	
	if ($rootScope.currentJsonIndex)
		$scope.Catagories = $rootScope.Catagories[$rootScope.currentJsonIndex].subcategories;
	else
		$scope.Catagories = [];
	//$rootScope.Catagories = $scope.Catagories;
	
		$scope.params = 
		{
			"cat" : $scope.ItemId,
			"subcat" : "",
			"location_lat" : "",
			"location_lng" : "" 			
		};

		
	$scope.BlurSearch = function()
	{
		$ionicScrollDelegate.scrollTop();
	}	
	
	
	$scope.selectCat = function(index,id)
	{
		$scope.params.subcat = id;	
		$scope.currentId = id;
		$scope.getSuppliers(id);
	}

	
	$scope.getSuppliers = function(id)
	{
		$scope.oldId = id;
		//$scope.params.subcat = id;	

		
		var optionsPosition = 
		{
		  enableHighAccuracy: false,
		  //timeout: 5000,
		  maximumAge: 0
		};
		
		navigator.geolocation.getCurrentPosition($scope.successLocation,$scope.errorLocation, optionsPosition);
	}
	
	
	$scope.successLocation = function(pos)
	{
		$scope.params.location_lat = pos.coords.latitude;
		$scope.params.location_lng = pos.coords.longitude
		
		$scope.doSearchFun();

	}
	
	$scope.errorLocation = function(err)
	{
		$scope.params.location_lat = "";
		$scope.params.location_lng = "";	
		$scope.doSearchFun();
	}


	
	$scope.doSearchFun = function()
	{
		
		SendPostToServer($scope.params,$rootScope.LaravelHost+'/GetSuppliersByCat',function(data, success) 
		{
			$scope.SuppliersArray = data;
			console.log("scope.SuppliersArray: ", data)
		});		
	}
	
	
	
	if ($scope.ItemId > 0)
		$scope.getSuppliers($scope.ItemId);
	else
		$scope.SuppliersArray = $rootScope.SearchMainArray;
	
	$scope.navigateCard = function(item)
	{
		window.location ="#/app/details/"+item.subdomain;
		console.log(item);
		//window.location ="#/app/shopIntro/"+item.index;
	}
	
	$scope.getDetails = function(item)
	{
		

		console.log(" STID -----" , item)
		
			GetShopById.loadShopById(item.index,$rootScope.pushId).then(function(data)
			{
				console.log("Data11 : " , $rootScope.getCustomerById);
				if ($rootScope.getCustomerById.response.status == 0)
				{
					$ionicPopup.alert({
					 title: 'קוד חנות שגוי יש לנסות שוב',
					buttons: [{
						text: 'אשר',
						type: 'button-positive',
					  }]
				   });	
					
					$scope.store.id = '';
				}
				else
				{
					console.log("GetShopById:" , data)
					//$localStorage.userid = '';
					
					$localStorage.custumerid = $rootScope.getCustomerById.response.custumerid;
					$localStorage.showmainpage	= $rootScope.getCustomerById.response.showmainpage;
					$localStorage.custumerimage = $rootScope.getCustomerById.response.custumerimage;
					$localStorage.custumername = $rootScope.getCustomerById.response.custumername;	
					$localStorage.custumersubdomain = $rootScope.getCustomerById.response.subdomain;

					$localStorage.shopId = item.index;
					$localStorage.manager = "0";
					$localStorage.usertype = "0";	
					$scope.data.search = "";
					console.log(" Resp1 : " , $localStorage)
					//$state.go('app.shopIntro/'+$scope.store.id);
					window.location ="#/app/shopIntro/";	 
				}

				
				/*if(ShowMain == 0)
					$state.go('app.main');
				else
					window.location.href = "#/app/products_template2/0"
					
				if($localStorage.BasketArray  && $localStorage.BasketArray != '' )
				{
					$rootScope.BasketArray = $localStorage.BasketArray;
				}*/
			});	
	}
	
	$scope.formatNumber = function(i) {
		return Math.round(i * 100)/100; 
	}
	
	
})	


.controller('SearchMainCtrl', function($scope,$state,$rootScope,$localStorage,GetShopById,$stateParams,$ionicSideMenuDelegate,SendPostToServer,SetDetailstoArray,SwipeLeftRight,$ionicScrollDelegate,$ionicPopup) 
{
	$scope.host = $rootScope.Host+'/php/';

	$scope.MainSearchArray = $rootScope.SearchMainArray;
	
	$scope.BlurSearch = function()
	{
		$ionicScrollDelegate.scrollTop();
	}	
	
})		

.controller('DealsCtrl', function($scope,$rootScope,$stateParams,$ionicSideMenuDelegate,SendPostToServer,SetDetailstoArray,SwipeLeftRight,$ionicScrollDelegate) 
{
	$scope.host = $rootScope.Host+'/php/';
	$scope.ItemId = $stateParams.ItemId;

	$scope.getSupplierDeals = function()
	{
		params = 
		{
		  "supplierid" : $scope.ItemId
		};
		
				
		SendPostToServer(params, $rootScope.LaravelHost+'/GetDealsBySupplierId',function(data, success) 
		{
			$scope.SupplierDeals = data;
		});		
	}
	
	$scope.getSupplierDeals();
	
	
	$scope.cutDate = function(date)
	{
		var dArray = date.split('-');
		dateStr = dArray[0]+"."+dArray[1]
		return dateStr;
	}
	
	$scope.cutYear = function(date)
	{
		var dArray = date.split('-');
		dateStr = dArray[2]
		return dateStr
	}		
})	
	
	
.controller('DetailsCtrl', function($scope,$rootScope,$stateParams,$ionicSideMenuDelegate,SendPostToServer,SetDetailstoArray,SwipeLeftRight,$ionicPopup,ClosePopupService,GetShopById,$localStorage) 
{
	$scope.ItemId = $stateParams.ItemId;	
	$scope.Sub = $scope.ItemId;
	$scope.host = $rootScope.Host;
	$scope.Details = "";
	$scope.DetailsArray = "";
	$scope.contentStyle = {};
	$scope.Family = '';
	
	$scope.params = 
	{
		"sub" : $scope.Sub,
		"location_lat" : "",
		"location_lng" : ""
	};	

	

	
	$scope.settings2 = 
	{
		"cardType":1,
		"pattern":1,
		"iconType":2,
		"iconColor":2,
		"fontColor":1,
		"fontSize":20,
		"fontFamily":1,
		"lineColor":2,
		"lineType":1
	}


	
	
	$scope.successLocation = function(pos)
	{
		$scope.params.location_lat = pos.coords.latitude;
		$scope.params.location_lng = pos.coords.longitude
		$scope.getDetailsServer();		
	}
	
	$scope.errorLocation = function()
	{
		$scope.params.location_lat = "";
		$scope.params.location_lng = "";	
		$scope.getDetailsServer();
	}


	$scope.getLocation = function()
	{
		
		var optionsPosition = {
		  enableHighAccuracy: false,
		  //timeout: 5000,
		  maximumAge: 0
		};
		
		navigator.geolocation.getCurrentPosition($scope.successLocation, $scope.errorLocation, optionsPosition);
	}
	
	$scope.getLocation();

	
	
	$scope.getDetailsServer = function()
	{

		
		HostUrl = $rootScope.LaravelHost+'/GetDetails'	
				
		SendPostToServer($scope.params,HostUrl,function(data, success) 
		{
			$scope.Details = data[0];
			console.log("Details: ",$scope.Details)
			
			$scope.settings = 
			{
				"cardType":$scope.Details.cardType,
				"pattern":$scope.Details.pattern,
				"iconType":$scope.Details.iconType,
				"iconColor":$scope.Details.iconColor,
				"fontColor":$scope.Details.fontColor,
				"fontSize":$scope.Details.fontSize,
				"fontFamily":$scope.Details.fontFamily,
				"lineColor":$scope.Details.lineColor,
				"lineType":$scope.Details.lineType,
			}
			
			


			
			//$scope.contentStyle['background-color'] = $scope.Details.bcolor;
			$scope.contentStyle['color'] = $rootScope.fontColors[$scope.settings.fontColor];
			$scope.contentStyle['font-size'] = $scope.settings.fontSize + "px";
			$scope.Family = $rootScope.fonts[$scope.settings.fontFamily-1];
			
			SetDetailstoArray($scope.Details,$scope.GetDetails)
			console.log("Details: ", data);
		});		
	} 	

	
	$scope.GetDetails = function(data)
	{
		$scope.DetailsArray = data;
	}
	
	$scope.goStore = function()
	{
		
		GetShopById.loadShopById($scope.Details.index,$rootScope.pushId).then(function(data)
		{
			console.log("GetShopById:" , data)
			//$localStorage.userid = '';
			$localStorage.custumerid = $rootScope.getCustomerById.response.custumerid;
			$localStorage.showmainpage	= $rootScope.getCustomerById.response.showmainpage;
			$localStorage.custumerimage = $rootScope.getCustomerById.response.custumerimage;
			$localStorage.custumername = $rootScope.getCustomerById.response.custumername;	
			$localStorage.custumersubdomain = $rootScope.getCustomerById.response.subdomain;
			$localStorage.shopId = $scope.Details.index;
			$localStorage.manager = "0";
			$localStorage.usertype = "0";	
			console.log(" Resp1 : " , $localStorage)
			//$state.go('app.shopIntro/'+$scope.store.id);
			window.location ="#/app/shopIntro/";

		});		

	}
	
	$scope.showError = function(errorText)
	{
		$ionicPopup.alert({
		 title: errorText,
			buttons: [{
			text: 'אשר',
			type: 'button-positive',
		  }]
	   });			
	}
	
	$scope.openPage = function(index)
	{
		if (index == 0) {
			if ($scope.Details.phone)
				window.location.href="tel://"+$scope.Details.phone;
			else
				$scope.showError("לא הוזן טלפון");
		}

		if (index == 1) {
			if ($scope.Details.business_lat)
				window.location = "waze://?q="+$scope.Details.business_lat+","+$scope.Details.business_lng+"&navigate=yes";
			else
				$scope.showError("לא הוזן כתובת");
		}
		
		if (index == 2) {
			if ($scope.Details.facebook)
				iabRef = window.open($scope.Details.facebook, '_blank', 'location=yes');
			else
				$scope.showError("לא הוזן פייסבוק");
		}
		
		if (index == 3) {
			if ($scope.Details.contactmail)
				window.location = "mailto:"+$scope.Details.contactmail;	
			else
				$scope.showError("לא הוזן מייל");
		}
		
		if (index == 4) {
			if ($scope.Details.website)
				iabRef = window.open($scope.Details.website, '_blank', 'location=yes');	
			else
				$scope.showError("לא הוזנו פרטים");
		}

		if (index == 8) {
			if ($scope.Details.instagram)
				iabRef = window.open($scope.Details.instagram, '_blank', 'location=yes');
			else
				$scope.showError("לא הוזנו פרטים");
		}
		
	}
	
	$scope.openSite = function(URL)
	{
		iabRef = window.open(URL, '_blank', 'location=yes');	
	}
	
	
	$scope.showImgPopUp = function (x) 
	{
		$scope.currentImage = x;
		myPopup = $ionicPopup.show({
			templateUrl: 'templates/img_popup.html',
			scope: $scope,
			cssClass: 'infoMainDiv'
		});

		ClosePopupService.register(myPopup);
			
	}	
	
	
	$scope.hideInfo = function ()
	{
         myPopup.close();
    };
	
	
	$scope.showDealPopup = function(index)
	{
		
		$scope.dealInfo = $scope.Details.deals[index];
		
		$scope.dealpopup = $ionicPopup.show({
			templateUrl: 'templates/deal_info_popup.html',
			scope: $scope,
			cssClass: 'infoMainDiv'
		});
		
		ClosePopupService.register($scope.dealpopup);
	}
	
	$scope.closeDealPopup = function()
	{
		$scope.dealpopup.close();
	}
	
	$scope.showProfolioPopup = function(item)
	{
		$scope.portfolioInfo = item;
		
		$scope.portfoliopopup = $ionicPopup.show({
			templateUrl: 'templates/portfolio_info_popup.html',
			scope: $scope,
			cssClass: 'infoMainDiv'
		});
		
		ClosePopupService.register($scope.portfoliopopup);		
	}
	
	$scope.closePortfolioPopup = function()
	{
		$scope.portfoliopopup.close();
	}

	
	$scope.shareCard = function()
	{
		window.plugins.socialsharing.share('במפה כרטיס ביקור של חברת '+$scope.Details.name+'', 'במפה', '', 'http://'+$scope.Details.subdomain+'.bamapa.co.il/');

	}
	
	
	$scope.formatNumber = function(i) {
		return Math.round(i * 100)/100; 
	}	
	
	
	
})	

.controller('RealEstateCtrl', function($scope,$rootScope,$stateParams,$ionicSideMenuDelegate,SendPostToServer,SetDetailstoArray,SwipeLeftRight,$ionicPopup,ClosePopupService,GetShopById,$localStorage) 
{
	$scope.ItemId = $stateParams.ItemId;	
	$scope.host = $rootScope.Host;
	
	$scope.getRealEstate = function()
	{
		params = {"id" : $scope.ItemId};
		
		HostUrl = $rootScope.LaravelHost+'/GetRealEstate'	
				
		SendPostToServer(params,HostUrl,function(data, success) 
		{
			$scope.diraData = data[0];

			console.log("Details2: ", data);
		});
	}
	
	$scope.getRealEstate();
	
	
	$scope.showImgPopUp = function (x) 
	{
		$scope.currentImage = x;
		myPopup = $ionicPopup.show({
			templateUrl: 'templates/img_popup.html',
			scope: $scope,
			cssClass: 'infoMainDiv'
		});

		ClosePopupService.register(myPopup);
			
	}	
	
	
	$scope.hideInfo = function ()
	{
         myPopup.close();
    };

	
})		
	
.controller('ManageCardCtrl', function($scope,$rootScope,$ionicSideMenuDelegate,ClosePopupService,SendPostToServer,SetDetailstoArray,SwipeLeftRight,$ionicPopup,$localStorage,$timeout,$cordovaCamera,$ionicModal) 
{
	$scope.Sub = $localStorage.subdomain; //"tapper";
	$scope.host = $rootScope.Host;
	$scope.Details = "";
	$scope.DetailsArray = "";
	$scope.contentStyle = {};
	$scope.Family = '';
	$scope.newlocation = '';
	$scope.editState = '';
	
	$scope.Limits = 
	{
		"cardType":3,
		"pattern":14,
		"iconType":3,
		"iconColor":4,
		"fontColor":10,
		"fontSize":35,
		"fontFamily":3,
		"lineColor":6,
		"lineType":2,
	}
	
	$scope.settings = 
	{
		"cardType":1,
		"pattern":1,
		"iconType":2,
		"iconColor":2,
		"fontColor":1,
		"fontSize":20,
		"fontFamily":1,
		"lineColor":2,
		"lineType":1
	}
	
	$scope.sharefields =
	{
		"name" : "",
		"phone" : ""
	}		
	
	$scope.dealfields = 
	{
		"title" : "",
		"desc" : "",
		"oldprice" : "",
		"price" : "",
		"date" : "",
		"image" : "",
	}

	 	
	$scope.params = 
	{
		"sub" : $scope.Sub,
		"location_lat" : "",
		"location_lng" : ""		
	};
	
	
	$scope.successLocation = function(pos)
	{
		$scope.params.location_lat = pos.coords.latitude;
		$scope.params.location_lng = pos.coords.longitude
		$scope.getDetailsServer();		
	}
	
	$scope.errorLocation = function()
	{
		$scope.params.location_lat = "";
		$scope.params.location_lng = "";	
		$scope.getDetailsServer();
	}


	$scope.getLocation = function()
	{
		
		var optionsPosition = {
		  enableHighAccuracy: false,
		  //timeout: 5000,
		  maximumAge: 0
		};
		
		navigator.geolocation.getCurrentPosition($scope.successLocation, $scope.errorLocation, optionsPosition);
	}
	
	$scope.getLocation();

	
	
	$scope.getDetailsServer = function()
	{
		HostUrl = $rootScope.LaravelHost+'/GetDetails'	
		
		SendPostToServer($scope.params,HostUrl,function(data, success) 
		{
			$scope.Details = data[0];
			console.log("Details 1: " , data);
			console.log($scope.Details );
			
			$scope.settings = 
			{
				"cardType": parseInt($scope.Details.cardType),
				"pattern":parseInt($scope.Details.pattern),
				"iconType":parseInt($scope.Details.iconType),
				"iconColor":parseInt($scope.Details.iconColor),
				"fontColor":parseInt($scope.Details.fontColor),
				"fontSize":parseInt($scope.Details.fontSize),
				"fontFamily":parseInt($scope.Details.fontFamily),
				"lineColor":parseInt($scope.Details.lineColor),
				"lineType":parseInt($scope.Details.lineType),
			}
			
			

			
			//$scope.contentStyle['background-color'] = $scope.Details.bcolor;
			$scope.contentStyle['color'] = $rootScope.fontColors[$scope.settings.fontColor];
			$scope.contentStyle['font-size'] = $scope.settings.fontSize + "px";
			$scope.Family = $rootScope.fonts[$scope.settings.fontFamily-1];
			
			SetDetailstoArray($scope.Details,$scope.GetDetails)
		});		
	}
	

	

	
	
	var myPopup = "";
	
	$scope.openEditPopup = function (x) 
	{
		
			$scope.editState = x;
			$scope.htmlText = ''
			

			
			switch ($scope.editState) 
			{
				case 1:
					$scope.htmlText = "עריכת שם עסק";
					break; 
				case 2:
					$scope.htmlText = "עריכת תיאור העסק";
					break; 
				case 3:
					$scope.htmlText = "עריכת טלפון העסק";
					break; 	
				case 4:
					$scope.htmlText = "עריכת כתובת העסק";
					break; 	
				case 5:
					$scope.htmlText = "עריכת כתובת דף פייסבוק";
					break; 		
				case 6:
					$scope.htmlText = "עריכת אתר הבית";
					break; 	
				case 7:
					$scope.htmlText = "עריכת כתובת מייל";
					break; 	
				case 8:
					$scope.htmlText = "עריכת אינסטגרם";
					break; 						
			}


			myPopup = $ionicPopup.show({
                templateUrl: 'templates/title_popup.html',
                scope: $scope,
                cssClass: 'infoMainDiv'
            });

            //ClosePopupService.register(myPopup);	
	}
	
	

	
	$scope.showImgPopUp = function (x) 
	{
			$scope.currentImage = x;
			myPopup = $ionicPopup.show({
                templateUrl: 'templates/img_popup.html',
                scope: $scope,
                cssClass: 'infoMainDiv'
            });

            ClosePopupService.register(myPopup);
			
	}
	
	$scope.hideInfo = function ()
	{
         myPopup.close();
    };
	
	
	$scope.openNewDealPopup = function()
	{
		dealPopup = $ionicPopup.show({
			templateUrl: 'templates/deal_popup.html',
			scope: $scope,
			cssClass: 'infoMainDiv'
		});

		//ClosePopupService.register(dealPopup);		
	}
	
	$scope.hideDealPop = function()
	{
		dealPopup.close();
	}
	
	
	
	$scope.deleteGalleryImage= function(index,id)
	{
	   var confirmPopup = $ionicPopup.confirm({
		 title: 'האם לאשר מחיקה?',
		 template: ''
	   });
	
	   confirmPopup.then(function(res) {
		if(res) 
		{
			params = 
			{
				"user" : $localStorage.userid,
				"id" : id
			};
				
						
			SendPostToServer(params,$rootScope.LaravelHost+'/DeleteGalleryImage',function(data, success) 
			{
				$scope.Details.galleryimages.splice(index, 1);	
			});	
		} 

	   });	
	}
	
	
	$scope.sendDetails = function ()
	{ 
		
		if ($scope.Details.address != null)
		{
			if ($scope.Details.address.formatted_address)
			{
				
				$scope.locationaddress = String($scope.Details.address.geometry.location);
				$scope.split = $scope.locationaddress.split(",");
				$scope.locationx = $scope.split[0].replace("(", "");
				$scope.locationy = $scope.split[1].replace(")", "");
				$scope.Details.business_lat = $scope.locationx;
				$scope.Details.business_lng = $scope.locationy;
				$scope.Details.address = $scope.Details.address.formatted_address;
			}
			else
			{
				$scope.Details.address = $scope.Details.address;
				$scope.Details.business_lat = $scope.Details.business_lat;
				$scope.Details.business_lng = $scope.Details.business_lng;
			}
		}
		

		params = 
		{
			"user" : $localStorage.userid,
			"details_array" : $scope.Details
		};
			
					
		SendPostToServer(params,$rootScope.LaravelHost+'/UpdateCardRow',function(data, success) 
		{
			console.log("new details :", $scope.Details)
			console.log ("saved: " , data);
			$scope.hideInfo();
		});

	}

	
	
			
	$scope.GetDetails = function(data)
	{
		$scope.DetailsArray = data;
	}
	
	$scope.Swipe = function(side)
	{
		SwipeLeftRight(side,$scope.settings,$scope.Limits,function(data, success) 
		{
			$scope.settings = data;
			$scope.contentStyle['color'] = $rootScope.fontColors[$scope.settings.fontColor];
			$scope.contentStyle['font-size'] = $scope.settings.fontSize + "px";
			$scope.Family = $rootScope.fonts[$scope.settings.fontFamily-1];
			console.log($scope.Family + " : "  + $scope.settings.fontFamily)
			
			console.log("changes: " , data);
			
			params = 
			{
				"user" : $localStorage.userid,
				"sub" : $scope.Sub,
				"settings" : data
			};
			
					
			SendPostToServer(params,$rootScope.LaravelHost+'/SaveSettings',function(data, success) 
			{
				console.log ("saved: " , data);
			});


	
			
		});
	}
	
	$scope.saveDeal = function()
	{
		if ($scope.dealfields.title =="")
		{
			$ionicPopup.alert({
			 title: 'יש להזין כותרת הדיל',
			 template: ''
		   });				
		}
		else if ($scope.dealfields.desc =="")
		{
			$ionicPopup.alert({
			 title: 'יש להזין תיאור הדיל',
			 template: ''
		   });				
		}
		/*
		else if ($scope.dealfields.oldprice =="")
		{
			$ionicPopup.alert({
			 title: 'יש להזין מחיר ישן',
			 template: ''
		   });				
		}
		*/
		else if ($scope.dealfields.price =="")
		{
			$ionicPopup.alert({
			 title: 'יש להזין מחיר הדיל',
			 template: ''
		   });				
		}
		
		else if ($scope.dealfields.date =="")
		{
			$ionicPopup.alert({
			 title: 'יש להזין תאריך הדיל',
			 template: ''
		   });				
		}		
		
		else if ($scope.dealfields.image =="")
		{
			$ionicPopup.alert({
			 title: 'יש להעלות תמונה לדיל',
			 template: ''
		   });				
		}
		

		
		else
		{
			
			//$scope.newdate = dateFilter($scope.deal.dealdate, 'dd-MM-yyyy');
			
			
			params = 
			{
				"user" : $localStorage.userid,
				"sub" : $scope.Sub,
				"title" : $scope.dealfields.title,
				"desc" : $scope.dealfields.desc,
				"oldprice" : $scope.dealfields.oldprice,
				"price" : $scope.dealfields.price,
				"image" : $scope.dealfields.image
			};

						
			SendPostToServer(params,$rootScope.LaravelHost+'/AddNewDeal',function(newitem, success) 
			{
				$scope.Details.deals.push({
					"index" : newitem.index,
					"title" : $scope.dealfields.title,
					"desc" : $scope.dealfields.desc,
					"oldprice" : $scope.dealfields.oldprice,
					"price" : $scope.dealfields.price,
					"date" : $scope.dealfields.date,
					"image" : $scope.dealfields.image,
				});	
				
					$scope.dealfields.title = '';
					$scope.dealfields.desc = '';
					$scope.dealfields.oldprice = '';
					$scope.dealfields.price = '';
					$scope.dealfields.date = '';
					$scope.dealfields.image = '';

			});	

			
			$scope.hideDealPop();
		}
	}
	
	$scope.shareOptions = function()
	{
		window.plugins.socialsharing.share($localStorage.name+' שיתף איתך כרטיס ביקור', 'במפה', '', 'http://'+$scope.Sub+'.bamapa.co.il/');
		
	}
	
	$scope.deleteDeal= function(index,id)
	{
	   var confirmPopup = $ionicPopup.confirm({
		 title: 'האם לאשר מחיקה?',
		 template: ''
	   });
	
	   confirmPopup.then(function(res) {
		if(res) 
		{
			params = 
			{
				"user" : $localStorage.userid,
				"id" : id
			};
				
						
			SendPostToServer(params,$rootScope.LaravelHost+'/DeleteDeal',function(data, success) 
			{
				$scope.Details.deals.splice(index, 1);	
			});	
		} 

	   });	
	}
	
	$scope.toggleLeftSideMenu = function() 
	{
    	$ionicSideMenuDelegate.toggleLeft();
  	};
	
	$scope.autocompleteOptions = {
	  componentRestrictions: { country: 'IL' }
	}
	
	$scope.CameraOptions = function(type)
	{
		var myPopup = $ionicPopup.show({
		//template: '<input type="text" ng-model="data.myData">',
		//template: '<style>.popup { width:500px; }</style>',
		title: 'בחר/י אפשרות:',
		scope: $scope,
		cssClass: 'custom-popup',
		buttons: [


	   {
		text: 'מצלמה',
		type: 'button-positive',
		onTap: function(e) { 
		  $scope.takePicture(1,type);
		}
	   },
	   {
		text: 'גלריית תמונות',
		type: 'button-calm',
		onTap: function(e) { 
		 $scope.takePicture(0,type);
		}
	   },
	   	   {
		text: 'ביטול',
		type: 'button-assertive',
		onTap: function(e) {  
		  //alert (1)
		}
	   },
	   ]
	  });		
	}

	$scope.takePicture = function(index,type) 
	{
		var options;
		
		if (type == 0) // big main image
		{
			$scope.targetWidth = 686;
			$scope.targetHeight = 386;
		}

		else if (type == 1) //image 
		{
			$scope.targetWidth = 300;
			$scope.targetHeight = 300;
		}

		else if (type == 2) // card gallery
		{
			$scope.targetWidth = 700;
			$scope.targetHeight = 700;
		}

		else if (type == 3) //deal gallery
		{
			$scope.targetWidth = 1193;
			$scope.targetHeight = 775;
		}

		
		
		if(index == 1 )
		{

			options = { 
				quality : 75, 
				destinationType : Camera.DestinationType.FILE_URI,  //Camera.DestinationType.DATA_URL, 
				sourceType : Camera.PictureSourceType.CAMERA, 
				allowEdit : true,
				encodingType: Camera.EncodingType.JPEG,
				targetWidth: $scope.targetWidth,
				targetHeight: $scope.targetHeight,
				popoverOptions: CameraPopoverOptions,
				saveToPhotoAlbum: false,
				correctOrientation: true
			};
		
			
		}
		else
		{
			options = { 
				quality : 75, 
				destinationType : Camera.DestinationType.FILE_URI, 
				sourceType : Camera.PictureSourceType.PHOTOLIBRARY, 
				allowEdit : true,
				encodingType: Camera.EncodingType.JPEG,
				targetWidth: $scope.targetWidth,
				targetHeight: $scope.targetHeight,
				popoverOptions: CameraPopoverOptions,
				saveToPhotoAlbum: false,
				correctOrientation: true
			};
		}
		
        $cordovaCamera.getPicture(options).then(function(imageData) 
		{
			/*
			if(index == 1 )
			$scope.imgURI = "data:image/jpeg;base64," + imageData;
			else
			*/
			$scope.imgURI = imageData
			var myImg = $scope.imgURI;
			var options = new FileUploadOptions();
			options.mimeType = 'jpeg';
			options.fileKey = "file";
			options.fileName = $scope.imgURI.substr($scope.imgURI.lastIndexOf('/') + 1);
			//alert(options.fileName)
			
			//var headers={'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8; application/json'};
			//options.headers = headers;
			
			
			var params = {};
			params.user = $localStorage.userid;
			params.sub = $scope.Sub;
			options.params = params;
			var ft = new FileTransfer();
			ft.upload($scope.imgURI, encodeURI($rootScope.Host+'php/UploadImg1.php'), $scope.onUploadSuccess, $scope.onUploadFail, options);
    	});
		
		$scope.onUploadSuccess = function(data)
		{

			 $timeout(function() {
				 				 
				if (data.response) {

					if (type == 0)
						$scope.updateURL = 'UpdateProviderImage';
					
					else if (type == 1)
						$scope.updateURL = 'UpdateProviderImage';
					
					else if (type == 2)
						$scope.updateURL = 'NewSupplierImage';
					else
						$scope.updateURL = '';
					
					
					if (type == 3)
					{
						$scope.dealfields.image = data.response;
					}
					
					if ($scope.updateURL)
					{
						params = 
						{
							"user" : $localStorage.userid,
							"sub" : $scope.Sub,
							"image" : data.response,
							"type" : type,
						};
						
								
						SendPostToServer(params,$rootScope.LaravelHost+'/'+$scope.updateURL,function(newitem, success) 
						{
							if (type == 0)
								$scope.Details.mainimage = data.response;			
							
							else if (type == 1)
								$scope.Details.image = data.response;


							else if (type == 2)
							{
								$scope.Details.galleryimages.push({
									"index": newitem.index,
									"image": data.response
								});									
							}

						});							
					}
				}
			}, 300);
			

		}
		
		$scope.onUploadFail = function(data)
		{
			alert("onUploadFail : " + data);
		}
    }
	
	
	$scope.formatNumber = function(i) {
		return Math.round(i * 100)/100; 
	}		

	
})


.controller('HotDealsCtrl', function($scope,$rootScope,$ionicSideMenuDelegate,ClosePopupService,SendPostToServer,SetDetailstoArray,SwipeLeftRight,$ionicPopup,$localStorage,$timeout,$cordovaCamera,$ionicModal) 
{
	$scope.host = $rootScope.Host;
	
	$scope.params = 
	{
		"location_lat" : "",
		"location_lng" : ""
	}
	
	$scope.getLocation = function()
	{
		
		var optionsPosition = {
		  enableHighAccuracy: false,
		  //timeout: 5000,
		  maximumAge: 0
		};
			
		
		navigator.geolocation.getCurrentPosition($scope.successLocation, $scope.errorLocation, optionsPosition);						
							
	}
	
	$scope.getDeals = function()
	{
		SendPostToServer($scope.params,$rootScope.LaravelHost+'/GetDeals',function(data, success) 
		{
			$scope.DealsArray = data;
			console.log("DealsArray: ",data);
		});			
	}
	
	$scope.successLocation = function(pos)
	{
		$scope.params.location_lat = pos.coords.latitude;
		$scope.params.location_lng = pos.coords.longitude;
		$scope.getDeals();		
	}

	$scope.errorLocation = function(pos)
	{
		$scope.params.location_lat = "";
		$scope.params.location_lng = "";
		$scope.getDeals();				
	}

	
	$scope.getLocation();
	
	
	$scope.formatNumber = function(i) {
		return Math.round(i * 100)/100; 
	}	

})





.controller('shopIntroCtrl', function($scope,$rootScope,$ionicSideMenuDelegate,ClosePopupService,SendPostToServer,SetDetailstoArray,SwipeLeftRight,$ionicPopup,$localStorage,$timeout,$cordovaCamera,$ionicModal,$ionicLoading,GetShopById) 
{
	$localStorage.custumerid = $localStorage.shopId;
	$localStorage.showmainpage	= 0;
	$localStorage.custumerimage = '';
	$localStorage.custumername = 'tresor paris';	
	$localStorage.manager = "0";
	$localStorage.usertype = "0";	
	$scope.store = '';
	$scope.store.id = '';
				
	$scope.BasketPrice = 0;
	$scope.host = $rootScope.ShopHost; 
	$scope.navTitle= '<img class="title-image" src='+$rootScope.Host+$localStorage.custumerimage+' style="width:140px; margin-left:-15px; margin-top:5px;"/>';
	$rootScope.ProductsArray = [];
	
	$ionicLoading.show({
      template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
    });

	if (!$localStorage.custumerid)
	{
		console.log("GoBack")
	}	
					
	$scope.$on('imageloaded', function(events, args){
		$ionicLoading.hide();
	})
	
	$rootScope.getShopCatagories();
	 
	if ($localStorage.showmainpage != 0 && $localStorage.custumerid)
	{
	 	window.location.href = "#/app/shopProducts/0"
	}
  
	if ($rootScope.CatagoriesLoaded == 0)
	  $scope.getCatagories();
			
	$rootScope.$watch('catArray', function(value) 
	{
		 $scope.catArray = $rootScope.catArray;
		 $scope.updateBasket();
		  
		  if ($localStorage.showmainpage != 0 && $localStorage.custumerid)
		  {
			window.location.href = "#/app/shopProducts/0"
		  }
	  
	});		
		 
	$scope.updateBasket = function()
	{
		//$scope.BasketPrice  = 0;
		for(var i=0;i< $rootScope.BasketArray.length;i++)
		{
			$scope.BasketPrice += parseInt($rootScope.BasketArray[i].new_price)*$rootScope.BasketArray[i].quan;
		}
			
	}
		
	$scope.updateBasket();
		
	$scope.goProductPage = function(Cat,index)
	{
		window.location.href = "#/app/shopProducts/"+index;
	}
	
	

	
	$scope.exampleStore = function()
	{
		console.log(" SHOP " ,$localStorage.custumerid )
		GetShopById.loadShopById($localStorage.custumerid,$rootScope.pushId).then(function(data)
		{
			console.log("Data11 : " , data);
			
			
			console.log("Data11 : " , $rootScope.getCustomerById);

				//$localStorage.userid = '';
				$localStorage.custumerid= $rootScope.getCustomerById.response.custumerid;
				$localStorage.showmainpage	= $rootScope.getCustomerById.response.showmainpage;
				$localStorage.custumerimage = $rootScope.getCustomerById.response.custumerimage;
				$localStorage.custumername = $rootScope.getCustomerById.response.custumername;	
				$localStorage.manager = "0";
				$localStorage.usertype = "0";	
				$scope.store.id = '';
		});		
	}
	
	//$scope.exampleStore();
	
	
	$scope.goStore = function()
	{
		if ($localStorage.custumersubdomain)
			window.location.href = "#/app/details/"+$localStorage.custumersubdomain;
	}
})



.controller('shopProductsCtrl', function($scope,$rootScope,$ionicSideMenuDelegate,ClosePopupService,SendPostToServer,SetDetailstoArray,SwipeLeftRight,$ionicPopup,$localStorage,$timeout,$cordovaCamera,$ionicModal,$ionicLoading,$http,$stateParams,$ionicSideMenuDelegate,$ionicScrollDelegate) 
{
	$ionicSideMenuDelegate.canDragContent(false);
	$scope.userType = $localStorage.usertype;
	
	$timeout(function()
	{
		 $ionicScrollDelegate.scrollTo(0, $rootScope.positionTop, false);
    },100);
	

	$scope.iCounter = 0;
	
	$scope.getScrollPosition = function()
	{
		$scope.iCounter++;	
	}

	$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';


	$scope.gotoDetailsPage = function(product,index,category)
	{
		$rootScope.positionTop = $ionicScrollDelegate.$getByHandle('scroller').getScrollPosition().top;
		window.location.href = "#/app/shopDetails/"+product+"/"+index+"/"+category;
	}
	
   // $scope.$on('$ionicView.enter', function(e) {
	$scope.Category = $stateParams.ItemId;
	$scope.NextCategoryIndex = parseInt($stateParams.ItemId)+1;
	$scope.OldCategoryIndex = parseInt($scope.Category)-1;
	$scope.host = $rootScope.ShopHost; 
	$scope.CatagoryName = "";
	$scope.itemBasket = [];
	$scope.BasketPrice = 0;
	$scope.navTitle= '<p style="width:140px; margin-left:-15px; margin-top:5px;">'+$scope.CatagoryName +'</p>';
	$scope.catArray  = $rootScope.catArray;
	$scope.productsearch = '';
	$scope.ProductsArray = [];
	$scope.imagesArray = [];
	$scope.currentArray = [];
	$scope.AllProducts = [];
	$scope.checkExists = 1;
	
	$scope.arrayPlace = "";
	
	$ionicLoading.show({
      template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
    });
	
	
	$ionicSideMenuDelegate.canDragContent(false);
				
	$scope.$on('imageloaded', function(events, args)
	{
		$ionicLoading.hide();
	})
	
	/*$rootScope.$ionicGoBack = function() 
	{
		window.location.href = "#/app/main";
	}; */

	$scope.showCatagories= function()
	{
		$ionicSideMenuDelegate.toggleRight();
	}

	$scope.$watch('productsearch', function(value) 
	{
		
			if($scope.productsearch.length > 0)
				$scope.currentArray = $scope.AllProducts;
			else if($rootScope.catArray[$scope.Category])
				$scope.currentArray = $rootScope.catArray[$scope.Category].products;
	});
	 
	
	$scope.changePosition = function(type)
	{
		$rootScope.positionTop = 0;
		
		$timeout(function()
		{
			  $ionicScrollDelegate.scrollTo(0, $rootScope.positionTop, false);
			//window.scrollTo(0, 1200);
		},300);
		 $ionicScrollDelegate.scrollTo(0, 0, false);
		$scope.newCat = $scope.catArray[$scope.NextCategoryIndex];
		$scope.previousIndex = parseInt($scope.NextCategoryIndex)-1;
		$scope.oldCat = parseInt($scope.NextCategoryIndex)-1;
		$scope.oldIndex = $scope.catArray[$scope.oldCat];
		
		
		if (type == 1)
		{
			if ($scope.catArray.length > $scope.NextCategoryIndex  )
			{
				$scope.changeCatagory($scope.newCat.index,$scope.NextCategoryIndex);
			}

			
		}
		else
		{
			if ($scope.oldCat > 0)
			{
				$scope.changeCatagory($scope.oldIndex.index,$scope.OldCategoryIndex);
			}
		}
	}

	$scope.loadProducts = function(type)
	{
		if (type == 1)
			$rootScope.loadOnce = 0;
		
		$scope.AllProducts = [];
		$scope.ProductsArray = $rootScope.catArray[$scope.Category];
		
		for(i=0;i<$rootScope.catArray.length;i++)
		{
			var PR = $rootScope.catArray[i].products;
			for(j=0;j<PR.length;j++)
			{
				if (!PR[j].quan)
					PR[j].quan = PR[j].minorder;
				if (!PR[j].remarks)	
					PR[j].remarks = "";
						
				$scope.AllProducts.push(PR[j])
			}
		}
		
		$rootScope.AllProducts = $scope.AllProducts;
		if($scope.ProductsArray)
		{
			//if($scope.ProductsArray.products.length == 0)
			$ionicLoading.hide();
		}else
		{
		//	window.location.href = "#/app/intro";
			//return;
		}
		
		
	
		
		if($scope.ProductsArray)
		{
			$scope.CatagoryName = $scope.ProductsArray['name'];
//			console.log("$scope.CatagoryName" , $scope.CatagoryName)	
			$scope.navTitle= '<p style="width:240px !important; margin-left:-15px; margin-top:5px; background-color:red">'+$scope.CatagoryName +'</p>';
		}
		
		
		
//		console.log("$rootScope.catArray",$scope.ProductsArray)
		
		
		for(var i=0;i< $scope.ProductsArray.products.length;i++)
		{
			//$scope.ProductsArray[i].isClicked = false;
			if ($scope.ProductsArray.products[i].quan > 0)
			{						
				$scope.ProductsArray.products[i].quan = $scope.ProductsArray.products[i].quan;
			}
			else
			{
				$scope.ProductsArray.products[i].quan = $scope.ProductsArray.products[i].minorder;
			}
				
			if (!$scope.ProductsArray.products[i].remarks)	
				$scope.ProductsArray.products[i].remarks = "";						

		
			if ($scope.ProductsArray.products[i].color)
			{
				for(var j=0;j< $scope.ProductsArray.products[i].color.length;j++)
				{
					
					if (!$scope.ProductsArray.products[i].color[j].selected)
					$scope.ProductsArray.products[i].color[j].selected = "0";
					
				}
			}

			if ($scope.ProductsArray.products[i].size)
			{
				for(var g=0;g< $scope.ProductsArray.products[i].size.length;g++)
				{
					
					if (!$scope.ProductsArray.products[i].size[g].selected)
					$scope.ProductsArray.products[i].size[g].selected = "0";
					
				}
			}	
		}
				
//				console.log("product array : " , $scope.ProductsArray)
				
		//	}
			
			$scope.currentArray = $scope.ProductsArray.products;
			$rootScope.currentArray = $scope.currentArray;
			
//			console.log("$scope.currentArray", $scope.currentArray)
	//	}					
//			console.log("CT",$rootScope.catArray)
		
		
	

		$rootScope.loadOnce++;
		//alert ($rootScope.loadOnce);
	}

	$rootScope.$watch('catArray', function(value) 
	{
		$scope.catArray = $rootScope.catArray;
		
		if($scope.catArray.length > 0)
		$scope.loadProducts();
	});


	
	$scope.changeCheckBox = function(index,item,state)
	{
		if (state == 1)
		{
			item.isClicked = 0;
			item.quan = "1";
		}		
		else
		{
			item.isClicked = 1;
			//$scope.updateBasket();
		}
		console.log("basket new : ", $rootScope.BasketArray)	
		
		$scope.AddBasket(index,item);
		
	}

	$scope.goProductPage = function(Cat,index)
	{
		
		console.log("Cat" , index)
		$scope.Category = index;
		$scope.ProductsArray = $rootScope.catArray[$scope.Category];
		$scope.currentArray = $scope.ProductsArray.products;
		$rootScope.currentArray = $scope.currentArray;
	}
	
	$scope.AddBasket = function(index,item)
	{
		console.log("Basket" , index ,  item)
		//$scope.itemBasket = [];
		$scope.BasketPrice = 0;
		
		if ($rootScope.BasketArray.length == 0)
		$rootScope.BasketArray = new Array();
		
		
		if (item.isClicked == 1)
		$rootScope.BasketArray.push(item);
		else
		{
			item.isClicked = 0;
			for(var i=0;i< $scope.BasketArray.length;i++)
			{
				if ($scope.BasketArray[i].index == item.index)
				{
					$scope.BasketArray.splice(i, 1);
				}
			}
			
		}
		
		$scope.updateBasket();
	}



	$scope.updateBasket = function()
	{
		$scope.BasketPrice  = 0;
		console.log("basket: " , $rootScope.BasketArray);
		for(var i=0;i< $rootScope.BasketArray.length;i++)
		{
			$scope.BasketPrice += parseInt($rootScope.BasketArray[i].new_price)*$rootScope.BasketArray[i].quan;
		}
		
	}

	$scope.updateBasket();

	$scope.CartAdd = function()
	{
		console.log($rootScope.BasketArray);
		window.location.href = "#/app/orderform";
	}
	
	$scope.goToBasket = function()
	{
		console.log('basket: ', $scope.itemBasket);
		
		if ($scope.BasketPrice == 0)
		{
			$ionicPopup.alert({
			 title: 'יש תחילה להוסיף מוצרים לסל',
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
		   });	
		}
		else
		{
			window.location.href = "#/app/basket";
		}
	}

	
	
	$scope.ShowImageModal = function(image)
	{
		$scope.productImage = $rootScope.ShopHost+image;
		console.log("Modal : " + $scope.productImage )
		$ionicModal.fromTemplateUrl('image-modal.html', {
		  scope: $scope,
		  animation: 'slide-in-up'
		}).then(function(modal) {
		  $scope.modal = modal;
		  $scope.modal.show();
		 });
	}
	
	$scope.closeModal = function()
	{
		$scope.modal.hide();
	}


	$scope.changeQuan = function(type,item,arrayindex)
	{
		$scope.checkExists = 1;
		
		console.log("basket: ",$rootScope.BasketArray);
		

		for(var i=0;i< $rootScope.BasketArray.length;i++)
		{
			if ($rootScope.BasketArray[i].index == item.index)
			{
				$scope.checkExists = 0;
				$scope.arrayPlace = i;
				
			}
			else
			{
				
			}
			
		}			

			
		if (type == 1)
		{
			item.quan++;
			
			if ($scope.checkExists == 0)
			{
				//alert ("exists");
			}
			else
			{
				$rootScope.BasketArray.push(item);
			}
			
			$scope.updateBasket();
			
		}
		else
		{
			
			if (item.quan > item.minorder)
			{
				item.quan = String(parseInt(item.quan)-1);
			}
			else 
			{
				$scope.minorder = parseInt(item.minorder)+1;
				
				$ionicPopup.alert({
				 title: 'לא ניתן להזמין פחות ממינימום הזמנה '+$scope.minorder+' פריטים',
				buttons: [{
					text: 'אשר',
					type: 'button-positive',
				  }]
			   });	
			}
			
			
			if (item.quan == 0 && $scope.checkExists == 0)
			{
				//alert ($scope.arrayPlace);
				$scope.BasketArray.splice($scope.arrayPlace, 1);

			}

			$scope.updateBasket();

		}
	}

})


.controller('shopDetailsCtrl', function($scope,$rootScope,$ionicSideMenuDelegate,ClosePopupService,SendPostToServer,SetDetailstoArray,SwipeLeftRight,$ionicPopup,$localStorage,$timeout,$cordovaCamera,$ionicModal,$state,$ionicLoading,$http,$stateParams,$ionicSideMenuDelegate,$ionicScrollDelegate) 
{
	$scope.host = $rootScope.ShopHost; 
	$scope.navTitle = '<img class="title-image" src='+$rootScope.ShopHost+$localStorage.custumerimage+' style="width:140px; margin-left:-15px; margin-top:5px;"/>';
	
	console.log($scope.navTitle)
	$scope.TypeId = $stateParams.Type;
	$scope.ProductId = $stateParams.ItemId;
	$scope.CatagoryId = $stateParams.CatagoryId;
	//alert ($scope.CatagoryId);
	$scope.BasketPrice = 0;
	$scope.Size = "";
	$scope.Color = "";
	$scope.Subject = "";
	$scope.colorsArray = [];
	$scope.sizesArray = [];
	$scope.SubjectArray = [];
	$scope.controllerName = "shopDetailsCtrl";
	$scope.PrdQuan = "1";

	
	//alert ($scope.ProductId);
	
	$ionicLoading.show({
      template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
    });
	

  $scope.$on('imageloaded', function(events, args){
	$ionicLoading.hide();
  })
	$ionicLoading.hide();
	
	/*$rootScope.$ionicGoBack = function() {
		//alert (123);
		if ($state.current.name == "app.details")
		{
			if ($scope.CatagoryId == "-1")
			{
				window.location.href = "#/app/basket";
			}
			else
			{
				window.location.href = $rootScope.productPage+$scope.CatagoryId;
			}			
		}
		else
		{
			window.location.href = "#/app/main";
		}
	};  */


	$scope.$on('$destroy', function() {
		//$scope.ProductData.quan = 7;
		//alert ($scope.CatagoryId);
		//var oldSoftBack = $rootScope.$ionicGoBack;
		 //$rootScope.$ionicGoBack = oldSoftBack;
		 //alert (123);
	});

	
	$scope.Settings = function()
	{
		$scope.isAlert = 0;
//		console.log("Flag2")
		if ($scope.CatagoryId == "-1")
			$scope.ProductData = $rootScope.BasketArray[$scope.ProductId];
		else
		{
			//$scope.ProductData = $rootScope.currentArray[$scope.ProductId];
			for(i=0;i<$rootScope.AllProducts.length;i++)
			{
				//console.log("Len : " + $rootScope.AllProducts[i].index + " : "  +  $scope.ProductId)
				if($rootScope.AllProducts[i].index == $scope.ProductId)
				{
					$scope.ProductData = $rootScope.AllProducts[i];
					//console.log("Quan = " , $scope.ProductData)
				}
			}
		}
		console.log($scope.ProductData)
		//alert ($scope.ProductData.minorder)
		if ($scope.ProductData.quan > 0)
		{
			$scope.PrdQuan = $scope.ProductData.quan;
			//$scope.ProductData.quan = parseInt($scope.ProductData.minorder)+1;
			
		}
		else
		{
			$scope.PrdQuan = 1;
			//$scope.ProductData.quan = $scope.ProductData.quan;
		}
			
			
		if ($scope.ProductData.color)
		{
			$scope.colorsArray = $scope.ProductData.color;
			$scope.isAlert = 1;
		}
		
		if ($scope.ProductData.size)
		{
			$scope.sizesArray = $scope.ProductData.size;
			$scope.isAlert = 1;
		}
	
		if ($scope.ProductData.subjects)
		{
			$scope.SubjectArray = $scope.ProductData.subjects;
			$scope.isAlert = 1;
		}
	}
	
	$scope.Settings();
	
	
	//alert ($scope.ProductData.minorder)
	
	
	



	
//	console.log("ProductData",$scope.ProductData)
	
	$scope.changeQuan = function(type)
	{
		if (type == 1)
		{
			console.log("q1:" , $scope.ProductData.quan)
			$scope.ProductData.quan = String(parseInt($scope.ProductData.quan)+1);
			console.log("q2: ",$scope.ProductData.quan)
			$scope.PrdQuan = $scope.ProductData.quan;
		}
		else
		{
			if ($scope.ProductData.quan > $scope.ProductData.minorder)
			{
				$scope.ProductData.quan = String(parseInt($scope.ProductData.quan)-1);
				$scope.PrdQuan = $scope.ProductData.quan;
			}
			else
			{
				$ionicPopup.alert({
				 title: 'לא ניתן להזמין פחות ממינימום הזמנה '+$scope.ProductData.minorder+' פריטים',
				buttons: [{
					text: 'אשר',
					type: 'button-positive',
				  }]
			   });	
			}
			
		}
	}
	
	$scope.changeColor = function(type,index)
	{
		for(var j=0;j< $scope.colorsArray.length;j++)
		{
			//$scope.colorsArray[j].selected = "0";
			$scope.ProductData.color[j].selected = "0";
		}
		
		//$scope.colorsArray[index].selected = "1";
		$scope.ProductData.color[index].selected = "1";
		$scope.Color = type;	
		//console.log("ProductData" , $scope.ProductData)
		
		
	}
	


	$scope.changeSize = function(type,index)
	{
		for(var g=0;g< $scope.sizesArray.length;g++)
		{
			//$scope.sizesArray[g].selected = "0";
			$scope.ProductData.size[g].selected = "0";
		}
			
		//$scope.sizesArray[index].selected = "1";
		$scope.ProductData.size[index].selected = "1";
		$scope.Size = type;	
		
	}
	
	$scope.changeSubject = function(type,index)
	{
		
		for(var s=0;s< $scope.SubjectArray.length;s++)
		{
			//$scope.colorsArray[s].selected = "0";
			$scope.ProductData.subjects[s].selected = "0";
		}
		
		//$scope.colorsArray[index].selected = "1";
		$scope.ProductData.subjects[index].selected = "1";

		
		$scope.Subject = type;	
	}

	
	if ($scope.CatagoryId == -1)
	{
		console.log("Flag1")
		if ($scope.colorsArray[0])
		{
			for(var s=0;s< $scope.colorsArray.length;s++)
			{
				if ($scope.colorsArray[s].selected == 1)
				{
					$scope.Color = $scope.colorsArray[s].index;
				}						
			}
		}
		
		
		if ($scope.sizesArray[0])
		{
			for(var s=0;s< $scope.sizesArray.length;s++)
			{
				if ($scope.sizesArray[s].selected == 1)
				{
					$scope.Size = $scope.sizesArray[s].index;
				}						
			}
		}


		if ($scope.SubjectArray[0])
		{
			for(var s=0;s< $scope.SubjectArray.length;s++)
			{
				if ($scope.SubjectArray[s].selected == 1)
				{
					$scope.Subject = $scope.SubjectArray[s].index;
				}						
			}
		}
	}
	
	$scope.resetProduct = function()
	{

		//$scope.selectedColors = 0;
		//$scope.selectedSizes = 0;
		//$scope.selectedOptions = 0;	

		
		if ($scope.colorsArray[0])
		{
			for(var j=0;j< $scope.colorsArray.length;j++)
			{
				$scope.ProductData.color[j].selected = "0";
			}	
		}
		


		if ($scope.sizesArray[0])
		{
			for(var j=0;j< $scope.sizesArray.length;j++)
			{
				$scope.ProductData.size[j].selected = "0";
			}			
		}			
		
		
		if ($scope.SubjectArray[0])
		{
			for(var j=0;j< $scope.SubjectArray.length;j++)
			{
				$scope.ProductData.subjects[j].selected = "0";
			}			
		}			
		
		$scope.Size = "";
		$scope.Color = "";
		$scope.Subject = "";
	}
	
	
	$scope.CartOptions = function() 
	{

		var myPopup = $ionicPopup.show({
		//template: '<input type="text" ng-model="data.myData">',
		//template: '<style>.popup { width:500px; }</style>',
		title: 'מוצר נוסף לעגלה בהצלחה , האם ברצונך להמשיך לסל קניות?',
		scope: $scope,
		cssClass: 'custom-popup',
		buttons: [


	   {
		text: 'המשך בקניות',
		type: 'button-positive',
		onTap: function(e) { 
		  window.location.href = "#/app/shopProducts/"+$scope.CatagoryId;;
		}
	   },
	   {
		text: 'הוסף מוצר נוסף',
		type: 'button-calm',
		onTap: function(e)
		{ 
		 	$scope.resetProduct();
		}

	   },
	   ]
	  });
	
	};	
	
	
	$scope.changeCheckBox = function(index,item,state)
	{
		if (state == 1)
		{
			item.isClicked = 0;
			item.quan = "1";
		}		
		else
		{
			item.isClicked = 1;
			//$scope.updateBasket();
		}
		console.log("basket new : ", $rootScope.BasketArray)	
		
		$scope.AddBasket(index,item);
		
	}
	
	$scope.checkOutBtn = function(item)
	{
		if ($scope.CatagoryId == -1)
			window.location.href = "#/app/basket"
		else
			$scope.AddBasket(item);
	}
	
	$scope.AddBasket = function(item)
	{
		item.quan = $scope.PrdQuan;
		
		$scope.ProductAdd = 0;
		$scope.ProductExists = 0;
		$scope.BasketPrice = 0;
	
		//$scope.selectedColors = 0;
		//$scope.selectedSizes = 0;
		//$scope.selectedOptions = 0;	

	

		//validate colors
			if ($scope.colorsArray[0])
			{
				$scope.selectedColors = 0;
				//alert ($scope.ProductData.color.length);
				for(var i=0;i< $scope.ProductData.color.length;i++)
				{
					if ($scope.ProductData.color[i].selected == 1)
					{
						$scope.selectedColors = 1;
					}
				}
				
			}
			else
			{
				$scope.selectedColors = 1;
			}

			
			//validate sizes
			if ($scope.sizesArray[0])
			{
				$scope.selectedSizes = 0;
				//alert ($scope.ProductData.size.length);
				for(var i=0;i< $scope.ProductData.size.length;i++)
				{
					if ($scope.ProductData.size[i].selected == 1)
					{
						$scope.selectedSizes = 1;

					}
				}
			}
			else
			{
				$scope.selectedSizes = 1;
			}



			
			//validate options
			if ($scope.SubjectArray[0])
			{
				$scope.SelectedName = $scope.SubjectArray[0].subject_name;
				$scope.selectedOptions = 0;
				//alert ($scope.ProductData.subjects.length);
				for(var i=0;i< $scope.ProductData.subjects.length;i++)
				{
					if ($scope.ProductData.subjects[i].selected == 1)
					{
						$scope.selectedOptions = 1;
					}
				}
				
			}
			else
			{
				$scope.selectedOptions = 1;
			}
			

		
		if ($rootScope.BasketArray.length == 0)
		{
			$scope.ProductExists = 0;
			$scope.ProductAdd = 1;		
		}
		else
		{
			for(var i=0;i< $rootScope.BasketArray.length;i++)
			{
				if ($rootScope.BasketArray[i].index == item.index)
				{
					$scope.ProductExists = 1;
				}
				else
				{
					$scope.ProductExists = 0;
					$scope.ProductAdd = 1;					
				}
			}	
		}		

			if ($scope.ProductAdd == 1 || $scope.ProductExists == 1)
			{
				if ($scope.selectedColors == 0)
				{
					$ionicPopup.alert({
					 title: 'יש לבחור צבע',
					buttons: [{
						text: 'אשר',
						type: 'button-positive',
					  }]
				   });						
				}
				else if ($scope.selectedSizes == 0)
				{
					$ionicPopup.alert({
					 title: 'יש לבחור גודל',
					buttons: [{
						text: 'אשר',
						type: 'button-positive',
					  }]
				   });						
				}
				else if ($scope.selectedOptions == 0)
				{
					$ionicPopup.alert({
					 title: 'יש לבחור '+$scope.SelectedName,
					buttons: [{
						text: 'אשר',
						type: 'button-positive',
					  }]
				   });						
				}				
				else
				{
					item.isClicked = 1;
					var newItem = angular.copy(item);
					$rootScope.BasketArray.push(newItem);
					$localStorage.BasketArray = $rootScope.BasketArray;	
					
					if($scope.isAlert == 1)
					$scope.CartOptions();
					else
					window.location.href = $rootScope.productPage+$scope.CatagoryId;

				}
					
			}
		
		$scope.updateBasket();
	}

	
	$scope.ShowImageModal = function(image)
	{
		$scope.productImage = $rootScope.ShopHost+image;
		
		$ionicModal.fromTemplateUrl('image-modal.html', {
		  scope: $scope,
		  animation: 'slide-in-up'
		}).then(function(modal) {
		  $scope.modal = modal;
		  $scope.modal.show();
		 });
	}
	
	$scope.closeModal = function()
	{
		$scope.modal.hide();
	}	
	
	$scope.updateBasket = function()
	{		
		for(var i=0;i< $rootScope.BasketArray.length;i++)
		{
				$scope.BasketPrice += parseInt($rootScope.BasketArray[i].new_price)*$rootScope.BasketArray[i].quan;
		}	

			$scope.TotalPrice = $scope.DeliveryPrice+$scope.BasketPrice;

	}

	$scope.updateBasket();

	
	
})



.controller('BasketCtrl', function($scope, $stateParams,$http,$ionicPopup,$state,$localStorage,$rootScope,$ionicModal) {

//	console.log('BasketCtrl' ,$rootScope.BasketArray )
	$scope.host = $rootScope.ShopHost;
	$scope.BasketPrice = 0;
	$scope.BasketArray = $rootScope.BasketArray;
	$scope.navTitle= '<img class="title-image" src='+$rootScope.ShopHost+$localStorage.custumerimage+' style="width:140px; margin-left:-15px; margin-top:5px;"/>';
	$scope.canCheckOut = 0;
//	console.log('BasketCtrl2' ,$scope.BasketArray)
	$scope.discount = $localStorage.discount;
	$scope.timeStamp = Date.now(); 

	

		

	$scope.fields = 
	{
		"pickup" : "",
		"dest" : $localStorage.address,
		"deliverytype" : "",
		"remarks" : "",
		"deliverydate" : $scope.prettyDate,
		"store" : ""
	}


	$scope.stores = [];

	for(var i=0;i< $scope.BasketArray.length;i++)
	{
			$scope.BasketArray[i].isClicked = "1";
	}
	
		
	$scope.userType = $localStorage.usertype;
	
/*	
	$scope.getStores = function()
	{
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

		get_data = 
		{
			"custumerid" : $localStorage.custumerid,
			"agentid" : $localStorage.agentid,
			"user" : $localStorage.userid
		}

		$http.post($rootScope.Host+'/get_user_stores.php',get_data).success(function(data)
		{
			$scope.stores = data;
			
			if ($scope.stores.length ==  0)
			{
				$scope.fields.store = $localStorage.custumerid;
				$scope.canCheckOut  = 1;
			}
			
			console.log("stores: " , $scope.stores)
		});			
	}
*/
	if ($scope.userType == 1)
	{
		//$scope.getStores();
	}

					
		
	$scope.checkoutBtn = function()
	{
		window.location.href = "#/app/orderform";	
	}
		

	$scope.deleteProduct = function(index)
	{
		$scope.BasketArray[index].isClicked = false;
		$scope.BasketArray.splice(index, 1);
		$scope.updateBasket();
		//console.log($scope.ProductsArray);
	}


	$scope.changeQuan = function(type,index,item)
	{
		if (type == 1)
		{
			$scope.BasketArray[index].quan = String(parseInt($scope.BasketArray[index].quan)+1);
		}
		else
		{
			
			if ($scope.BasketArray[index].quan > $scope.BasketArray[index].minorder)
			{
				$scope.BasketArray[index].quan = String(parseInt($scope.BasketArray[index].quan)-1);
			}
			else
			{
				$ionicPopup.alert({
				 title: 'לא ניתן להזמין פחות ממינימום הזמנה '+$scope.BasketArray[index].minorder+' פריטים',
				buttons: [{
					text: 'אשר',
					type: 'button-positive',
				  }]
			   });					
			}
				
		}
		
		$scope.updateBasket();
		
		
		
	}

	
	$scope.ShowImageModal = function(image)
	{
		$scope.productImage = $rootScope.Host+image;
		
		$ionicModal.fromTemplateUrl('image-modal.html', {
		  scope: $scope,
		  animation: 'slide-in-up'
		}).then(function(modal) {
		  $scope.modal = modal;
		  $scope.modal.show();
		 });
	}
	
	$scope.closeModal = function()
	{
		$scope.modal.hide();
	}	

	
	$scope.updateBasket = function()
	{
		$scope.BasketPrice = 0;
		for(var i=0;i< $rootScope.BasketArray.length;i++)
		{
				$scope.BasketPrice += parseInt($rootScope.BasketArray[i].new_price)*$rootScope.BasketArray[i].quan;
		}
		
	}

	$scope.updateBasket();
	
	$scope.changeCheckBox = function(index,item,type)
	{
		var confirmPopup = $ionicPopup.confirm({
		 title: 'האם לאשר מחיקת מוצר מהעגלה?'

	   });
	   confirmPopup.then(function(res) {
		 if(res) 
		 {
			$scope.BasketArray[index].quan = "1";
			$scope.BasketArray[index].isClicked = "0";
			$scope.BasketArray.splice(index, 1);
			$scope.updateBasket();
		 } 
	   });
	   
	}
	
	$scope.AddBasket = function(index,item)
	{
		$scope.BasketArray[index].quan = item.quan;
		$scope.updateBasket();

	}

	$scope.checkExtras= function(item)
	{
		if (item)
		{
			$scope.CountSelected = 0;
			for(var i=0;i< item.length;i++)
			{
				//if (item[i].isClicked == 1)
					if (item[i].selected == 1)
					$scope.CountSelected++;
			}	
			
			if ($scope.CountSelected > 0)
				return true;
			else
				return false;			
		}

	}

	$scope.checkLoggedIn = function()
	{
			if ($rootScope.BasketArray.length == 0)
			{
				$ionicPopup.alert({
				 title: 'סל קניות ריק'  ,
				buttons: [{
					text: 'אשר',
					type: 'button-positive',
				  }]
			   });				
			}
			else
			{
				if ($localStorage.shopuserid)
				{
					$scope.completeOrder();
				}	
				else
				{
				   var confirmPopup = $ionicPopup.confirm({
					 title: 'יש להתחבר בכדי לבצע הזמנה',
					 template: 'האם ברצונך להתחבר/להרשם?'
				   });

				   confirmPopup.then(function(res) {
					 if(res) {
					   window.location.href = "#/app/loginshop";
					 } else {
					   //return;
					 }
				   });					
				}	

			}
	}
    
	
	$scope.completeOrder = function()
	{
	
		console.log("userType ",$scope.userType )
		if ($scope.userType == 1)
		{
			$scope.storeId = $rootScope.selectedStore;
			$scope.userId = $rootScope.selectedStore;
			//$scope.userId =  $localStorage.userid;
			console.log("shop : " , $scope.userId)
			//
			if ($rootScope.selectedStore =="")
			{
				//alert (444);
				$ionicPopup.alert({
				 title: 'יש לבחור חנות'  ,
				buttons: [{
					text: 'אשר',
					type: 'button-positive',
				  }]
			   });		

				$state.go('app.selectstore');
			}
			else
			{
				$scope.canCheckOut = 1;
			}
			
		}				
		else
		{
			$scope.storeId = $localStorage.custumerid;
			$scope.userId = $localStorage.userid;
			$scope.canCheckOut = 1;
		}
		
		if ($scope.canCheckOut == 1)
		{

			$state.go('app.payment');
						
		}			
			
	}
	

	

	$scope.showDetails = function()
	{
		console.log("basket: ",$scope.BasketArray)
		
	}

	if ($scope.discount)
	{
		$scope.priceAfterDiscount = $scope.BasketPrice-(($scope.discount*$scope.BasketPrice)/100);
		//alert ($scope.priceAfterDiscount)
	}
	else
	{
		$scope.priceAfterDiscount  = $scope.BasketPrice;
	}	
})


.controller('PaymentCtrl', function($scope, $stateParams,$http,$ionicPopup,$state,$localStorage,$rootScope,$ionicModal) {

	$scope.host = $rootScope.ShopHost;
	$scope.BasketPrice = 0;
	$scope.BasketArray = $rootScope.BasketArray;
	$scope.navTitle= '<img class="title-image" src='+$rootScope.ShopHost+$localStorage.custumerimage+' style="width:140px; margin-left:-15px; margin-top:5px;"/>';
	$scope.canCheckOut = 0;
	$scope.discount = $localStorage.discount;
	$scope.timeStamp = Date.now(); 

	
	$scope.doPayment = function()
	{

		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

	//					console.log("basket array ", $rootScope.BasketArray)
		//return;
		if ($rootScope.basketId =="")
			$rootScope.basketId = $scope.timeStamp;
		
			//alert ($rootScope.basketId)
		
			//alert(JSON.stringify($rootScope.BasketArray));
			checkout_data = 
			{
				
				"custumerid" : $localStorage.custumerid,
				"agentid" : $localStorage.agentid,
				//"user" : $localStorage.userid,
				"user" : $localStorage.shopuserid,//  $scope.userId,
				//"pickup" : $scope.fields.pickup,
				//"dest" : $scope.fields.dest,
				//"deliverytype" : $scope.fields.deliverytype,
				//"remarks" : $scope.fields.remarks,
				//"deliverydate" : $scope.fields.deliverydate,
				"sum" : $scope.BasketPrice,
				"products" : $rootScope.BasketArray,
				"cartId" : $rootScope.basketId,
			}
			
			//alert (777);
			
			console.log("cartParam : ",checkout_data);
			//return;
			$http.post($rootScope.ShopHost+'/checkout_cart.php',checkout_data).success(function(data)
			{
				//alert (555);
					console.log("cart1 : ",data);
					$rootScope.basketId = '';
					$scope.BasketPrice = 0;
					$scope.payAmount = 0;
					$scope.clearSelected();
					$rootScope.BasketArray = [];
					//$rootScope.BasketArray = new Array();


					$ionicPopup.alert({
					 title: 'תודה שהזמנת '+$localStorage.custumername+ ' ההזמנה נשלחה , נציג שירות יחזור אליך בהקדם'  ,
					buttons: [{
						text: 'אשר',
						type: 'button-positive',
					  }]
				   });	
				   
				   $localStorage.BasketArray = '';	
				   $rootScope.selectedStore = "";
				   $scope.storeName = "";
				   $scope.updateBasket();
				   window.location.href = "#/app/main";
				   $rootScope.selectedStore == "";
				   $scope.storeId = $rootScope.selectedStore;
				   $scope.canCheckOut = 0;
				   $rootScope.storeName = "";
			});
	}
	
	$scope.clearSelected = function()
	{
		for(var i=0;i< $rootScope.BasketArray.length;i++)
		{
				$rootScope.BasketArray[i].isClicked = false;
				$rootScope.BasketArray[i].quan = "0";
		}			
	}	

	
	$scope.updateBasket = function()
	{
		$scope.BasketPrice = 0;
		for(var i=0;i< $rootScope.BasketArray.length;i++)
		{
				$scope.BasketPrice += parseInt($rootScope.BasketArray[i].new_price)*$rootScope.BasketArray[i].quan;
		}
		
	}

	$scope.updateBasket();

	

})

.filter('stripslashes', function () {
    return function (value) 
	{
		if(value)
		{
			return (value + '')
			.replace(/\\(.?)/g, function(s, n1) {
			  switch (n1) {
				case '\\':
				  return '\\';
				case '0':
				  return '\u0000';
				case '':
				  return '';
				default:
				  return n1;
			  }
			});
		}
		else
		{
			return ("");
		}
    };
})

.filter('toTrusted', function ($sce) 
{
    return function (value) {
        return $sce.trustAsHtml(value);
    };
})

;