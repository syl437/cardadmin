angular.module('starter.factories', [])

.factory('SendPostToServer', SendPostToServer)
.factory('SetDetailstoArray', SetDetailstoArray)
.factory('SwipeLeftRight' , SwipeLeftRight)
.factory('ClosePopupService' , SwipeLeftRight)



//SendDetailsToServer

.factory('GetShopById', function($http,$rootScope,$localStorage) {
	var data = [];

	return {
		loadShopById: function(storeid,pushid){
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
			
			
		/*	data1 = 
			{
				"categoryId" : $localStorage.custumerid 
			}				
			return  $http.post($rootScope.Host+'/getCategory.php',data1)
			.success(function(data, status, headers, config)
			{
					$rootScope.catArray = data;
					console.log("catagories: " , data)
					$scope.catArray = $rootScope.catArray;
					$scope.updateBasket();
					$rootScope.CatagoriesLoaded++;
			});		  */
			
			data1 = 
			{
				"storeId" : storeid,
				"pushid" : pushid
			}

			//alert (pushid);
			//console.log("D1 : " , params)
			return $http.post($rootScope.ShopHost+'/getCustomerById.php',data1).then(function(resp)
			{
				console.log(" Resp : " , resp.data)
				$rootScope.getCustomerById = resp.data;
			});
		},
	}
})




.factory('ClosePopupService', function($document, $ionicPopup, $timeout){
        var lastPopup;
        return {
            register: function(popup) {
                $timeout(function(){
                    var element = $ionicPopup._popupStack.length>0 ? $ionicPopup._popupStack[0].element : null;
                    if(!element || !popup || !popup.close) return;
                    element = element && element.children ? angular.element(element.children()[0]) : null;
                    lastPopup  = popup;
                    var insideClickHandler = function(event){
                        event.stopPropagation();
                    };
                    var outsideHandler = function() {
                        popup.close();
                    };
                    element.on('click', insideClickHandler);
                    $document.on('click', outsideHandler);
                    popup.then(function(){
                        lastPopup = null;
                        element.off('click', insideClickHandler);
                        $document.off('click', outsideHandler);
                    });
                });
            },
            closeActivePopup: function(){
                if(lastPopup) {
                    $timeout(lastPopup.close);
                    return lastPopup;
                }
            }
        };
    });
	
	

function SendPostToServer($http,$rootScope,$ionicLoading) 
{
  return function(params,url,callback) 
  {
	  
		$ionicLoading.show({
		  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
		});	

				
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';	
		$http.post(url,params)
			.success(function(data, status, headers, config)
			{
				$ionicLoading.hide();
				callback(data, true);
			})
			.error(function(data, status, headers, config)
			{
				console.log("s2344545")
				$ionicLoading.hide();
				callback(config, false);
			});						
  }
}


function SetDetailstoArray($http,$rootScope) 
{
  return function(details,callback) 
  {
		var Arr = new Array();
		
		if(details.phone != "")
		{
			Obj = 
			{
				"title":"phone",
				"value":details.phone,
				"text":"חייג עכשיו",
				"dbrow" : "phone",
				"icon":"1"
			}
			
			Arr.push(Obj)
		}
		
		if(details.website != "")
		{
			Obj = 
			{
				"title":"website",
				"value":details.website,
				"text":"אתר הבית",
				"dbrow" : "website",
				"icon":"2"
			}
			
			Arr.push(Obj)
		}
		
		if(details.gallery != "")
		{
			Obj = 
			{
				"title":"gallery",
				"value":details.gallery,
				"text":"גלרייה",
				"dbrow" : "gallery",
				"icon":"3"
			}
			
			Arr.push(Obj)
		}
		
		if(details.desc != "")
		{
			Obj = 
			{
				"title":"about",
				"value":details.desc,
				"text":"אודות",
				"dbrow" : "desc",
				"icon":"4"
			}
			
			Arr.push(Obj)
		}
		
		if(details.contactmail != "")
		{
			Obj = 
			{
				"title":"contact",
				"value":details.contactmail,
				"text":"צור קשר",
				"dbrow" : "contactmail",
				"icon":"5"
			}
			
			Arr.push(Obj)
		}
		
		if(details.address != "")
		{
			Obj = 
			{
				"title":"waze",
				"value" : details.address,
				//"value":details.business_lng+","+details.business_lat,
				"text":"WAZE",
				"dbrow" : "address",
				"icon":"6"
			}
			
			Arr.push(Obj)
		}
		
		if(details.facebook != "")
		{
			Obj = 
			{
				"title":"facebook",
				"value":details.facebook,
				"text":"facebook",
				"dbrow" : "facebook",
				"icon":"7"
			}
			
			Arr.push(Obj)
		}
		
		if(details.youtube != "")
		{
			Obj = 
			{
				"title":"youtube",
				"value":details.youtube,
				"text":"youtube",
				"dbrow" : "youtube",
				"icon":"8"
			}
			
			Arr.push(Obj)
		}
		
		callback(Arr, false);
  }
}


function SwipeLeftRight($http,$rootScope) 
{
	return function(side,settings,Limits,callback) 
	{
		if($rootScope.currentSetting == 1)
		{
			if(Limits.pattern == settings.pattern)
			{
				settings.pattern = 0;
			}
			
			settings.pattern = settings.pattern + 1;
		}
		
		if($rootScope.currentSetting == 2)
		{
			if(Limits.iconType == settings.iconType)
			{
				settings.iconType = 0;
			}
			
			settings.iconType = settings.iconType + 1;
		}
		
		if($rootScope.currentSetting == 3)
		{
			if(Limits.fontColor == settings.fontColor)
			{
				settings.fontColor = 0;
			}
			
			settings.fontColor = settings.fontColor + 1;
		}
		
		if($rootScope.currentSetting == 4)
		{
			if(Limits.fontFamily == settings.fontFamily)
			{
				settings.fontFamily = 0;
			}
			
			settings.fontFamily = settings.fontFamily + 1;
		}
		
		if($rootScope.currentSetting == 5)
		{
			if(Limits.fontSize == settings.fontSize)
			{
				settings.fontSize = 0;
			}
			
			settings.fontSize = settings.fontSize + 1;
		}
		
		if($rootScope.currentSetting == 6)
		{
			if(Limits.lineColor == settings.lineColor)
			{
				settings.lineColor = 0;
			}
			
			settings.lineColor = settings.lineColor + 1;
		}
		
		if($rootScope.currentSetting == 7)
		{
			if(Limits.iconColor == settings.iconColor)
			{
				settings.iconColor = 0;
			}
			
			settings.iconColor = settings.iconColor + 1;
		}
		
		if($rootScope.currentSetting == 0)
		{
			if(Limits.cardType == settings.cardType)
			{
				settings.cardType = 0;
			}
			
			settings.cardType = settings.cardType + 1;
		}
		
		
		callback(settings);
	}
}


 










